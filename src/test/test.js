function DigitsVisualizer() {
    this.process = function (page) {
        $reactions.answer('Выберите цифру:');
        $reactions.buttons(page.map(function (digit) {
            return {text: digit + "", hide: true, transition: '/Test/Digit'}
        }));
    };
}

Memento.registerPojoObject(
    DigitsVisualizer,
    function () {
        return new DigitsVisualizer();
    }
);

function GoodsPageProcessor(buttons, items) {
    var self = this;

    self.items = items || {};
    self.buttons = buttons;


    self.process = function (page) {
        page.GoodsItemList.forEach(function (item) {
            self.items[item.Name] = item;
        });

        if (page.GoodsItemList.length === 1) {
            $.session.currentItemId = page.GoodsItemList[0].Id;
            visualiseGoodsDetails(page.GoodsItemList[0]);
        } else if (page.GoodsItemList.length > 1) {
            visualiseGoodsPage(page);
        } else {
            $reactions.transition('/Catalog/Search/DoSearch/NoResults');
        }
        
        if (self.buttons) {
            $reactions.buttons(self.buttons);
        }
    };
}

Memento.registerPojoObject(
    GoodsPageProcessor,
    function (pojo) {
        return _.extend(new GoodsPageProcessor(pojo.buttons, pojo.items), _.omit(pojo, 'items', 'buttons'));
    }
);



function CategoriesPageProcessor(items) {
    var self = this;

    self.items = items || {};

    self.process = function (page) {
        visualiseCategoriesPage(page);

        page.forEach(function (category) {
            self.items[category.Name] = category;
        });
    };
}

Memento.registerPojoObject(
    CategoriesPageProcessor,
    function (pojo) {
        return _.extend(new CategoriesPageProcessor(pojo.items), _.omit(pojo, 'items'));
    }
);