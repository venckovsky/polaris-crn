require: test.js

theme: /
    state: /Test
        q!: test
        a: test menu
        buttons:
            "search"    -> ./Search
            "catalog"   -> ./Catalog
            "paginator" -> ./Paginator
            "cart"      -> ./Cart
            "pending"   -> ./Pending

        state: Search || modal = true
            a: введите фразу для поиска
            script:
                paginator().reset();

            state: Exit
                q: exit
                go: /

            state: CheckButton
                q: *
                script:
                    var pageProcessor = paginator().getPageProcessor();
                    var item = pageProcessor && pageProcessor.items[$.request.query];

                    if (item) {
                        visualiseGoodsDetails(item);
                    } else {
                        $reactions.transition('/Test/Search/DoSearch');
                    }

                state: GoBackToSearch
                    q: назад
                    script:
                        paginator().currentPage();

            state: DoSearch
                script:
                    paginator().display({
                        exitState: '/Test/Search/CheckButton',
                        noMoreState: '/Test/Search/NoMore',
                        iterator: new SearchPageIterator($context.parseTree.text, {Count: 3}),
                        processor: new GoodsPageProcessor()
                    });

            state: NoMore
                a: Нет результатов


        state: Paginator
            a: paginator
            script:
                paginator().clear();
                paginator().display({
                    exitState: '/',
                    noMoreState: '/Test/Paginator/NoMore',
                    iterator: new ArrayPageIterator([1,2,3,4,5,6,7,8], 3),
                    processor: new DigitsVisualizer()
                    //moreButtonText: 'дальше'
                });
            state: NoMore || noContext = true
                a: Больше нет цифр
        state: Digit
            a: Вы выбрали: {{$request.query}}
            script:
                paginator().save();
                var arr = [1,2,3,4,5,6,7,8].map(function (d) {return d + '.' + $request.query});
                paginator().display({
                    exitState: '/Test/Digit',
                    noMoreState: '/Test/Paginator/NoMore',
                    iterator: new ArrayPageIterator(arr, 3),
                    processor: new DigitsVisualizer()
                });

            state: Back
                q: *назад*
                script:
                    paginator().restore();

        state: Cart
            a: Корзина. Комманды add, delete, clear, lookup. add (id товара) delete (id товара). Примеры id 19211862 29080677 60905196
            state: Add
                q: add $Text
                script:
                    var r = cart().add($parseTree.Text[0].value);
                    $reactions.answer(toPrettyString(r));

            state: Delete
                q: delete $Text
                script:
                    var r = cart().delete($parseTree.Text[0].value);
                    $reactions.answer(toPrettyString(r));

            state: Clear
                q: clear
                script:
                    var r = cart().clear();
                    $reactions.answer(toPrettyString(r));

            state: Lookup
                q: lookup
                script:
                    var r = cart().lookup();
                    $reactions.answer(toPrettyString(r));

        state: Catalog
            a: Категории товаров. Выбирай категории. Скажи "Ищи $товар" для того чтобы осуществить поиск по категории
            script:
                paginator().reset();
                paginator().clear();

                var categories = catalog().getCategories();
                if (categories.isSuccessfulApiCall()) {
                    var iterator = new ArrayPageIterator(categories.GoodsCategoryList, 5);

                    paginator().display({
                        exitState: '/Test/Catalog/Category',
                        noMoreState: '/Test/Catalog/NoMore',
                        iterator: iterator,
                        processor: new CategoriesPageProcessor()
                    });
                } else {
                    $reactions.answer('ошибка');
                }

            state: NoMore || noContext = true
                a: Нет больше категорий

            state: Back
                q: *назад*
                script:
                    paginator().restore();

            state: SearchInCategory
                q: Ищи $Text
                script:
                    var processor = paginator().getPageProcessor();
                    var categoryId = processor && processor.categoryId;

                    if (categoryId) {
                        var textToSearch = $parseTree.Text ? $parseTree.Text[0].value : '';

                        if (!paginator().containsState('categories')) {
                            paginator().save('categories');
                        }

                        var iterator = new SearchPageIterator(textToSearch, {Count: 3, GoodsCategoryId: categoryId});
                        var processor = new GoodsPageProcessor({text: 'назад к категориям'});
                        processor.categoryId = categoryId;

                        $reactions.answer('Вот что нашлось');

                        paginator().display({
                            exitState: '/Test/Catalog/SearchInCategory/CheckButton',
                            noMoreState: '/Test/Catalog/SearchInCategory/NoMore',
                            iterator: iterator,
                            processor: processor
                        });
                    }
                    else {
                        $reactions.answer('Не выбрана категория для поиска')
                    }

                state: BackToCategories
                    q: назад к категориям
                    script: paginator().restore('categories');
                    go: /Test/Catalog/Category

                state: CheckButton
                    q: *
                    script:
                        var pageProcessor = paginator().getPageProcessor();
                        var item = pageProcessor && pageProcessor.items[$.request.query];

                        if (item) {
                            visualiseGoodsDetails(item);
                        } else {
                            $reactions.transition('/');
                        }

                    state: GoBackToSearch
                        q: назад
                        script:
                            paginator().currentPage();

                state: NoMore
                    a: Нет результатов

            state: Category
                q: *
                script:
                    var pageProcessor = paginator().getPageProcessor();
                    var category = pageProcessor && pageProcessor.items[$.request.query];

                    if (category) {
                        if (category.HasChildren) {
                            var categories = catalog().getCategories(category.Id);
                            if (categories.isSuccessfulApiCall(category)) {
                                var iterator = new ArrayPageIterator(categories.GoodsCategoryList, 4);
                                var processor = new CategoriesPageProcessor();
                                processor.categoryId = category.Id;

                                paginator().save();
                                paginator().display({
                                    exitState: '/Test/Catalog',
                                    noMoreState: '/Test/Catalog/NoMore',
                                    iterator: iterator,
                                    processor: processor
                                })
                            }
                        } else {
                            $reactions.answer('Нет подкатегорий. Ищу товары в категории');
                            $reactions.transition('/Test/Catalog/SearchInCategory');
                        }
                    } else {
                        $reactions.answer('Не верно указана категория. Иду в /')
                        $reactions.transition('/');
                    }

        state: Pending
          a: calling 'echoPending' api method
          script:
            var apiResult = utkoClient().call({
              Method: 'echoPending'
            });

            $reactions.answer('Api result status: ' + apiResult.getApiStatus());

            if (apiResult.isPending()) {
              var pendingRequestKey = apiResult.getPendingRequestKey();
              $reactions.answer('Api apiResult.getPendingRequestKey(): ' + pendingRequestKey);


              var realResult = utkoClient().getPending(pendingRequestKey);

              // для echoPending статус будет success, но в реальности все еще может быть pending
              $reactions.answer('Real result status: ' + realResult.getApiStatus());
              // для echoPending результат будет пустым
              $reactions.answer('Real result: ' + JSON.stringify(realResult, null, 2));
            }
            else {
              $reactions.answer('Api result is not pending! Ooops');
            }
