patterns:
    $change = (менять|меняет|меняют|заменить|поменять|изменить|сменить|смени|~смена|~замена|нов*|преобраз*|не актуальн*|неактуальн*|другой|не мой)

    $mobilePhone = (номер*|сотов*|смартфон*|айфон*|[номер*|моб*] телефон*|моб.телефон*|мобильно*|(мобильник*|мобильн*|мобила|мобилы|мобилу|мобиле|сот*) (номер*|телефон*))

    $testMobilePhoneNumber = ($regexp<(70000)(.){6}>)

theme: /Authorisation

    state: test auth || noChangeState = true
        q!: start test auth
        script:
            user().authenticate('89406', '94075159');
        a: Тестовый юзер авторизован.    
    
    state: exit
        q!: log out
        script:
            user().deauthenticate();
        a: Вы вышли из вашего аккаунта.    

    state: PhoneNumber
        q!: start auth
        if: !user().isAuthenticated()
            go!: ./Ask
        else:
            a: Вы авторизованы. 
            go!: /Delivery/Start

        state: Ask 
            script:
                $reactions.answer({value: 'Пожалуйста, назовите номер своего мобильного телефона в формате 79...', tts: 'Пожалуйста, назовите номер своего мобильного телефона в формате семь девятьсот...'});
                $reactions.buttons([{text: 'Корзина', hide: true, transition: '/Cart/Show'}, {text: 'Каталог', hide: true, transition: '/Catalog/Main'}]);


            state: Change
                q!: * {$change *  $mobilePhone} * 
                a: Пожалуйста, назовите новый номер телефона.

            state: Valid
                q: * ($mobilePhoneNumber/$testMobilePhoneNumber) *
                q: * ($mobilePhoneNumber:changeNumber/$testMobilePhoneNumber:changeNumber) * || fromState = ../Change, onlyThisState = true
                script:
                    var phNum;
                    if (isYandex() && $request.rawRequest && $request.rawRequest.request && $request.rawRequest.request.command) {
                        phNum = $request.rawRequest.request.command.replace(/ /g, '');
                        } else {
                            if($parseTree.mobilePhoneNumber){
                                phNum = $parseTree._mobilePhoneNumber;
                            } else {
                                phNum = $parseTree._testMobilePhoneNumber;
                            }
                        }
                    if ($parseTree._Root === 'changeNumber' && $client.UtkonosPhoneNumber) {
                        if ($client.UtkonosPhoneNumber === phNum) {
                           $reactions.answer('Отлично! Я запомнила ваш номер.')
                        } else {
                            $client.UtkonosPhoneNumber = phNum;
                            $reactions.answer('Ваш номер изменён.')
                        }    
                    } else {
                        $client.UtkonosPhoneNumber = phNum.trim();
                    }
                go!: /Authorisation/GetCode    

            state: NotANumber
                q: * 
                q: * $Number *
                script:
                    $reactions.answer({value: 'Пожалуйста, назовите номер, который начинается с +79 и состоит из 11 цифр — или введите номер с клавиатуры.', tts: 'Пожалуйста, назовите номер, который начинается с плюс семь девятьсот и состоит из 11 цифр — или введите номер с клавиатуры.'});
                    $reactions.buttons([{text: 'Корзина', hide: true, transition: '/Cart/Show'}, {text: 'Каталог', hide: true, transition: '/Catalog/Main'}]);

    state: GetCode
        script:
            var result = user().requestAuthenticationCode($client.UtkonosPhoneNumber);
            processCodeRequestResult(result);

    state: AskCode
        script:
            $reactions.answer('Пожалуйста, введите код из SMS.');
            $reactions.buttons([{text: 'Не пришёл код', hide: true}, {text: 'Изменить номер', hide: true}]);

        state: Code
            q: * $repeat<$Number> *
            script:
                var code = '';

                if (isYandex() && $request.rawRequest && $request.rawRequest.request && $request.rawRequest.request.command) {
                    code = $request.rawRequest.request.command.replace(/ /g, '');
                } else {
                    var codeDct = $parseTree.Number;
                    for (var c = 0;c < codeDct.length;c++) { 
                        code += codeDct[c].value;
                    }
                }
                user().authenticate(code);
            if: user().isAuthenticated()
                a: Вы авторизованы.
                script:
                    var goodsAfterAuth;

                    if ($session.orderMode) {
                        $session.orderMode = false;
                        $reactions.transition('/Order/OrderHistory');
                    } else {
                        goodsAfterAuth = cart().lookup('update').CartList[0].Goods;
                        if (goodsAfterAuth && (goodsAfterAuth.length > $session.goodsQuantity)) {
                            $reactions.answer('В вашем аккаунте был неоформленный заказ, он добавлен в вашу текущую корзину.');
                            $reactions.transition('/Cart/Show');
                        } else {    
                            $reactions.transition('/Delivery/Start');
                        }
                    }    
            else: 
                script:
                    $reactions.answer('Неправильный код. Попробуйте ещё раз.');
                    $reactions.buttons([{text: 'Получить новый код', hide: true}]);
         

        state: No code
            q: * {ничего * (не (пришло/приходило)/нет)} *
            q: * {(нет/не пришел/не приходит/потерялся) * (код/кода)} * 
            q: * (пришли*/нужен/давай/получить) * [новый] код ещё раз *
            q: * (пришли*/нужен/давай/получить) * новый код *
            q: * (пришли/давай) (ещё/другой) *
            script:
                $temp.lostCode = true;
            go!: ../../GetCode