theme: /Order

    state: TEst
        q!: test
        script:
            $reactions.answer(dateIsPast("2018-07-05T15:00:00.000Z").toString());


    state: OrderAdd
        q: ($agree/подожду/жду/долго) || fromState = /Order/OrderAdd/Pending, onlyThisState = true
        script:
            var orderResult = order().add($session.userName, '+70000000', $session.fullAddress.street, $session.fullAddress.house, $session.fullAddress.flat);

            if (orderResult.isTimeout()) {
                $reactions.answer('Я отправила ваш заказ в службу доставки. Оплата при получении. Пожалуйста, убедитесь, что заказ появился в истории. Там вы узнаете его номер.');
                $reactions.buttons({text: 'История заказов', hide: true, transition: '/Order/OrderHistory'})
                    if (orderRes.isApiError()) {
                        $reactions.answer("Что-то пошло не так. Попробуйте изменить заказ.");
                        $reactions.buttons([{text: 'Изменить заказ', hide: true, transition: '/Cart/Show'}]);
                        }
                    }

        if: $session.deliveryTypeId != 3
            go!: ./OrderInfo
        else:
            go!: ./OrderPickup                   

        state: OrderInfo
            script:
                var DeliveryAddressFull = $session.fullAddress.street + ', д. ' + $session.fullAddress.house + ', кв. ' + $session.fullAddress.flat;
                var DeliveryAddresswFlat = $session.fullAddress.street + ', д. ' + $session.fullAddress.house;
                var DeliveryAddress = $session.fullAddress.flat == '-' ? DeliveryAddresswFlat : DeliveryAddressFull;
                $reactions.answer('Ваш заказ ' + $session.currentOrderId + ' оформлен. Заказ будет доставлен по адресу ' + DeliveryAddress + '. Оплата наличными.');
                $reactions.buttons([{text: 'Отменить заказ', hide: true, transition: '/Order/OrderCancel'},
                                    {text: 'Создать шаблон заказа', hide: true, transition: '/Templates/Add'},
                                    {text: 'История заказов', hide: true, transition: '/Order/OrderHistory'}]);

        state: OrderPickup
            script:
                $reactions.answer('Ваш заказ ' + $session.currentOrderId + ' оформлен. Заказ можно получить по адресу  г. Москва, ул. Кантемировская, д. 61.');
                $reactions.buttons([{text: 'Отменить заказ', hide: true, transition: '/Order/OrderCancel'},
                                    {text: 'Создать шаблон заказа', hide: true, transition: '/Templates/Add'},
                                    {text: 'История заказов', hide: true, transition: '/Order/OrderHistory'}]);


    state: Break
        q!: (не *жду/не буду * | * (отмен* заказ*) *)
        q: $disagree || fromState = /Order/OrderAdd/Pending
        q: $disagree || fromState = /Delivery/Intervals/GetDeliveryIntervals/Pending
        q: $disagree || fromState = /Delivery/Intervals/GetDeliveryIntervals/CatchAllTime

        q: Отменить заказ ||fromState = /Delivery/Start/Address
        script:
            $reactions.answer("Вы действительно хотите прервать оформление заказа?");
            $reactions.buttons([{text: 'Да', hide: true, transition: './Yes'}, {text: 'Нет', hide: true, transition: './No'}]);

        state: Yes
            q: $agree
            script:
                if ($session.currentOrderId) {
                    $reactions.transition('/Order/OrderCancel/Yes');
                } else {
                    $reactions.answer('Вы ещё не оформили заказ. Его статус можно проверить в истории заказов.');
                    $reactions.buttons([{text: 'История заказов', hide: true, transition: '/Order/OrderHistory'}, {text: 'Новый заказ', hide: true, transition: '/Catalog/Main'},  {text: 'Корзина', hide: true, transition: '/Cart/Show'}]);
                }
                
        state: No
            q: $disagree
            script:
                $temp.goods = cart().lookup();
                if ($temp.goods.CartList[0].Goods.length == 0) {
                    $reactions.answer('Я отправила ваш заказ в службу доставки. Оплата при получении. Пожалуйста, убедитесь, что заказ появился в истории. Там вы узнаете его номер.');
                    $reactions.buttons({text: 'История заказов', hide: true, transition: '/Order/OrderHistory'});
                } else {
                    $reactions.transition('/Cart/Show');
                }
    
    state: OrderCancel
        q: * (отмен* заказ*) * || fromState = /Order/OrderHistory, onlyThisState = true
        script:
            $reactions.answer("Вы действительно хотите отменить этот заказ?");
            $reactions.buttons([{text: 'Да', hide: true, transition: './Yes'}, {text: 'Нет', hide: true, transition: './No'}]);          

        state: Yes
            q: $agree
            script:
                var cancel = order().cancel($session.currentOrderId);
                if (!cancel.isApiError()) {
                    $reactions.answer('Ваш заказ ' + $session.currentOrderId + ' отменён.');
                } else {
                    $reactions.answer(cancel.getApiError());
                }
                $session.currentOrderId = null;
                $reactions.buttons([{text: 'Новый заказ', hide: true, transition: '/Catalog/Main'}, {text: 'История заказов', hide: true, transition: '/Order/OrderHistory'}]);
                
        state: No
            q: $disagree
            go!: /Order/OrderAdd/OrderInfo


    state: OrderHistory
        q!: * истор* заказ* *
        q!: * (мои/мой) заказ* * 
        q!: * раньше заказыв* * 
        q: $hurryUpCommands || fromState = ., onlyThisState=true
        script:
            if (user().isAuthenticated()) {
                var orders = order().search();
                if (orders.isTimeout()) {
                    $reactions.answer("{Секундочку/Немного терпения}! Я открываю историю заказов. Подождёте ещё чуть-чуть?");
                    $reactions.buttons({text: 'Да', hide: true, transition: '.'});
                } else {
                    var orders = orders.OrderList;
                    if (orders.length > 0) {
                    $reactions.answer('Ваша история заказов:');
                    
                    paginator().reset();
                    paginator().display({
                        exitState: '/Order/OrderHistory',
                        iterator: new ArrayPageIterator(orders),
                        processor: new OrdersPageProcessor([{text: 'Новый заказ', hide: true, transition: '/Catalog/Main'}])
                        });
                    } else {
                        $reactions.answer('Вы ещё не оформили ни одного заказа.');
                        $reactions.transition('/Catalog/Main');
                    }

                }
            } else {
                $session.orderMode = true;
                $reactions.transition('/Authorisation/PhoneNumber/Ask');
            }            


        state: GetOrderId 
            q: $regexp<\d+>
            script:
                var orderId = $request.query;
                $session.currentOrder = order().getOrder(orderId);
                $session.currentOrderId = $session.currentOrder.ID;
            go!: ../OrderLookup

        state: OrderLookup
            q: * ((законч*/хватит/прекратит*/внест*/завершить) [редактиров*/изменен*]) * || fromState = /Order/OrderModify, onlyThisState = true
            q: * (откр*/покаж*/показ*) (заказ) *  || fromState = /Order/OrderModify, onlyThisState = true
            script:
                try {
                    visualiseOrderLookup($session.currentOrder);
                } catch (e) {
                    log(e);
                    $reactions.answer('Не могу найти информацию об этом заказе.');
                }    

            state: GoBackToSearch
                q: назад
                q: * (не (то/это*/эту/эти*)/~другой/не нравится) * 
                script:
                    paginator().currentPage();

    state: Promocode
        q!: * промокод* *
        script:
            if ($session.promocode) {
                $reactions.answer('Ваш промокод: ' + $session.promocode + '. Вы можете изменить или удалить его.');
                $reactions.buttons([{text: 'Изменить', hide: true, transition: './AddPromocode'}, 
                                    {text: 'Удалить', hide: true, transition: './Deactivate'},
                                    {text: 'Корзина', hide: true, transition: '/Cart/Show'},
                                    {text: 'Каталог', hide: true, transition: '/Catalog/Main'}]);
            } else{
                $reactions.answer('Если у вас есть промокод, вы можете добавить его к заказу.');
                $reactions.buttons([{text: 'Корзина', hide: true, transition: '/Cart/Show'},
                                    {text: 'Каталог', hide: true, transition: '/Catalog/Main'}]);
                $reactions.transition('./AddPromocode');
            }

        state: AddPromocode
            q:  {* добав* [промокод]}
            script:
                $reactions.answer('Введите номер промокода, например, 123.');
                $reactions.buttons([{text: 'К заказу', hide: true, transition: '/Cart/Show'},
                                    {text: 'Каталог', hide: true, transition: '/Catalog/Main'}])

            state: PromocodeNumber
                q: * $Number *
                script:
                    var promocode = $parseTree.Number[0].value;
                    var promocodeUse = order().usePromocode(promocode);
                    if (promocodeUse.isSuccessfulApiCall()) {
                        $session.promocode = promocode;
                        $reactions.answer('Итак, ваш промокод -- ' + $session.promocode + '. Продолжить покупки?');
                        $reactions.buttons([{text: 'К заказу', hide: true, transition: '/Cart/Show'},
                                            {text: 'Каталог', hide: true, transition: '/Catalog/Main'}]);
                        }

            state: DoesNotMatchNumber
                q: * 
                a: Что-то не похоже на число. Попробуйте ещё раз.
                go: ../

        state: Deactivate
            q: * (удали*/деактивир*) * [промокод] *
            script:
                $reactions.answer('Удалить промокод?');
                $reactions.buttons([{text: 'Да', hide: true, transition: './Yes'},
                                    {text: 'Нет', hide: true, transition: './No'},
                                    {text: 'К заказу', hide: true, transition: '/Cart/Show'}])

            state: Yes
                q: $agree 
                script:
                    var deactivation = order().deactivatePromocode();
                    if (deactivation.isSuccessfulApiCall()) {
                        $session.promocode = null;
                        $reactions.answer('Вы деактивировали промокод.');
                        $reactions.buttons([{text: 'К заказу', hide: true, transition: '/Cart/Show'},
                                            {text: 'Каталог', hide: true, transition: '/Catalog/Main'}]);
                        }

            state: No
                q: $disagree
                go!: /Order/Promocode
