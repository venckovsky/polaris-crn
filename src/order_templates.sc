require: patterns.sc
  module = common

theme: /Templates

    state: Show
        q!: * шаблон* [заказ*] *
        script:
            var templates = OrderTemplates().getTemplatesList();
            var iterator = new ArrayPageIterator(templates.TemplatesList);
            paginator().display({
                exitState: '/Templates/Show',
                iterator: iterator,
                processor: new TemplatesPageProcessor()
                });

        state: GetTemplateId 
            q: *
            script:
                var templateName = $request.query;
                $session.currentTemplate = getTemplateIdByName(templateName, $session.TemplatesList);
                $session.currentTemplateId = $session.currentTemplate.ORDER_TEMPLATE_ID;
            go!: ../VisualiseTemplate

        state: VisualiseTemplate
            script:
                var template = OrderTemplates().orderTemplateGet($session.currentTemplateId);
                visualiseTemplate($session.currentTemplate);

            state: GoBackToSearch
                q: назад
                q: * (не (то/это*/эту/эти*)/~другой/не нравится) * 
                script:
                    paginator().currentPage();

    state: Add
        q: {(добав*/сдела*) * шаблон * [заказа]} || fromState = /Order/OrderInfo, onlyThisState=true
        q: {(добав*/сдела*) * шаблон * [заказа]} || fromState = /Order/Pickup, onlyThisState=true
        a: Пожалуйста, введите название шаблона.

        state: TemplateName
            q: $Text
            script:
                var add = OrderTemplates().addTemplate($session.currentOrderId, $parseTree._Text); 
                if (add.isSuccessfulApiCall()) {
                    $reactions.answer('Заказ добавлен в шаблоны.');
                    $reactions.buttons([{text: 'Шаблоны заказов', hide: true, transition: '/Templates/Show'}, {text: 'Мои заказы', hide: true, transition: '/Cart/Show'}, {text: 'Продолжить покупки', hide: true, transition: '/Catalog/Main'}]);
                }


    state: Remove
        a: Удалить шаблон "{{$session.currentTemplate.ORDER_TEMPLATE_NAME}}"?
        script:
            $reactions.buttons([{text: 'Да', hide: true, transition: './Yes'},
                                {text: 'Нет', hide: true, transition: './No'},
                                {text: 'Мои заказы', hide: true, transition: '/Order/OrderHistory'}])
        
        state: Yes
            q: $agree
            script:
                var removeTemplate = OrderTemplates().removeTemplate($session.currentTemplate.ORDER_TEMPLATE_ID);
                if (removeTemplate.isSuccessfulApiCall()) {
                    $session.currentTemplate = null;
                    $session.currentTemplateId = null;
                    $reactions.answer('Шаблон удалён.');
                    $reactions.buttons([{text: 'Шаблоны заказов', hide: true, transition: '/Templates/Show'}, {text: 'Мои заказы', hide: true, transition: '/Cart/Show'}, {text: 'Продолжить покупки', hide: true, transition: '/Catalog/Main'}]);
                }

        state: No
            q: $disagree
            go!: /Templates/Show

    
    state: CreateOrderWithTemplate
        a: Создать новый заказ на основе шаблона "{{$session.currentTemplate.ORDER_TEMPLATE_NAME}}"?
        script:
            $reactions.buttons([{text: 'Да', hide: true, transition: './Yes'},
                                {text: 'Нет', hide: true, transition: './No'},
                                {text: 'Мои заказы', hide: true, transition: '/Order/OrderHistory'}])

        state: Yes
            q: $agree
            script:
                var newOrder = OrderTemplates().makeOrderWithTemplateId($session.currentTemplate.id);
                if (newOrder.isSuccessfulApiCall()) {
                    var data = newOrder.getApiData(); 
                    $reactions.answer('Новый заказ ' + data.ID + ' оформлен. Подробную информацию можно узнать в истории заказов.' );
                    $reactions.buttons([{text: 'Шаблоны заказов', hide: true, transition: '/Templates/Show'}, {text: 'История заказов', hide: true, transition: '/Order/OrderHistory'}, {text: 'Продолжить покупки', hide: true, transition: '/Catalog/Main'}]);
                } else {
                    $reactions.answer('Не удалось оформить заказ. Попробуйте ещё раз.');
                    $reactions.buttons([{text: 'Шаблоны заказов', hide: true, transition: '/Templates/Show'}, {text: 'История заказов', hide: true, transition: '/Order/OrderHistory'}, {text: 'Продолжить покупки', hide: true, transition: '/Catalog/Main'}]);
                }

        state: No
            q: $disagree
            go!: /Templates/Show
        