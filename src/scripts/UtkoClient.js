function UtkoClient(session, temp, settings) {
    classCallCheck(this, UtkoClient);

    var self = this;

    var errorHandlers = [];
    var pendingHandlers = [];

    function parseResponseDataIfNeeded(response) {
        try {
            if (response.response.entity.contentType.value.indexOf("text") === 0) {
                response.data = JSON.parse(response.data);
            }
        } catch (e) {
        }
    }

    function checkMoreThanOneApiCall(params) {
        temp.requestsCounter = temp.requestsCounter || 0;
        if (++temp.requestsCounter > 1) {
            self.log("WARNING! THERE IS MORE THAN ONE CALL IN CURRENT REQUEST", params)
        }
    }

    self.call = function (params) {
        checkMoreThanOneApiCall(params);

        var timeBeforeApiCall = Date.now();

//        var options = createRequestOptions(params, settings);

        var response = $http.get(settings.apiUrl + params);
        
        var timeAfterApiCall = Date.now();

        temp.timeLastApiCall = timeAfterApiCall - timeBeforeApiCall;

        log("api call time " + temp.timeLastApiCall + " query " + params);
        
        self.log("response", response, null, null);

        parseResponseDataIfNeeded(response);

        var result = new ApiClientResult(response);

        self.log('result', result);
        
/*        if (!result.isHttpError()) {
            var data = result.getApiData();

            if (data && data.Head && data.Head.SessionToken) {
                session.token = data.Head.SessionToken;
            }
        }
*/
        if (result.isHttpError()) {
            self.log("http error", result.getHttpError());

            errorHandlers.forEach(function (h) {
                h.handler(result);
            })
        }

        if (result.isApiError()) {
            self.log("api error '" + result.getApiErrorString() + "'", result.getApiError());

            errorHandlers
                .filter(function (h) {
                    return h.handleApiError
                })
                .forEach(function (h) {
                    h.handler(result);
                })
        }

        var pendingRequestKey = result.getPendingRequestKey();
        if (pendingRequestKey) {
            self.log("pending result", pendingRequestKey);

            pendingHandlers.forEach(function (h) {
                h(result)
            })
        }

        return result;
    };

    self.getPending = function (pendingRequestKey) {
        var lookupResult = self.call({
            Method: 'responseLookup',
            Body: {
                PendingRequestKey: pendingRequestKey
            }
        });
        if (!lookupResult.isSuccessfulApiCall()) {
            return lookupResult;
        }

        if (lookupResult.Status !== "pending") {
            var httpQueryResult = lookupResult.getHttpQueryResult();
            httpQueryResult.data = httpQueryResult.data.Body.Response;

            return new ApiClientResult(httpQueryResult);
        }
    };

    self.getLastSessionToken = function () {
        return session.token;
    };

    self.addErrorHandler = function (handler, handleApiErrors) {
        if (!_.isFunction(handler)) {
            throw new TypeError("error handler must be a function!")
        }

        errorHandlers.push({handler: handler, handleApiError: !!handleApiErrors});
    };

    self.addPendingResultHandler = function (handler) {
        if (!_.isFunction(handler)) {
            throw new TypeError("pending result handler must be a function");
        }

        pendingHandlers.push(handler);
    };

    self.log = createServiceLogger("UTKONOS CLIENT", settings.logging);

    // ==== Debug functions =====

    self.setTimeoutForApiMethod = function (apiMethod, milliseconds) {
        session.timeouts = session.timeouts || {};

        if (milliseconds) {
            session.timeouts[apiMethod] = milliseconds;
        }
        else {
            delete session.timeouts[apiMethod]
        }
    };

    self.getTimeoutForApiMethod = function (apiMethod) {
        if (apiMethod && session.timeouts && session.timeouts[apiMethod]) {
            return session.timeouts[apiMethod];
        } else {
            if (typeof temp.timeLastApiCall != "undefined") {
                temp.timeLastApiCall += 50;
            } else {
                temp.timeLastApiCall = 0;
            }
            return settings.defaultHttpTimeout - temp.timeLastApiCall;
        }
    };

    self.clearTimeoutsForApiMethods = function () {
        delete session.timeouts;
    };

    self.getAllTimeouts = function () {
        return _.extend({}, session.timeouts);
    };
}

function ApiClientResult(httpQueryResult) {
    classCallCheck(this, ApiClientResult);

    function createApiResultProperties(self, httpQueryResult) {

        self.isTimeout = function () {
            return (httpQueryResult.error && httpQueryResult.error === "Read timed out");
        };

        self.getHttpQueryResult = function () {
            return httpQueryResult;
        };

        self.isSuccessfulApiCall = function () {
            return !self.isHttpError();
        };

        self.isHttpError = function () {
            return !httpQueryResult.isOk
        };

        self.isApiError = function () {
            return !self.isHttpError() && self.getApiStatus() !== "ok";
        };

        self.isPending = function () {
            return self.getApiStatus() === "pending";
        };

        self.getPendingRequestKey = function () {
            if (self.isPending()) {
                return self.getApiData().Body.PendingRequestKey
            }
        };

        self.getApiData = function () {
            return httpQueryResult.data;
        };

        self.getApiStatus = function () {
            var data = self.getApiData();
            if (data) {
                return data.status;
            }
        };

        self.getHttpQueryResult = function () {
            return httpQueryResult;
        };

        self.getHttpError = function () {
            return httpQueryResult.error;
        };

        self.getApiError = function () {
            if (httpQueryResult) {
                return httpQueryResult[0];
            }
        };

        self.getApiErrorString = function () {
            var e = self.getApiError();

            if (e) {
                return e.Message + " (" + e.Description + ")";
            }
        };

        self.getApiErrorCode = function () {
            var e = self.getApiError();

            if (e) {
                return e.Code;
            }
        };

        self.getApiErrorClass = function () {
            var e = self.getApiError();

            if (e) {
                return e.Class;
            }
        };

        self.getApiErrorDescription = function () {
            var e = self.getApiError();

            if (e) {
                return e.Description;
            }
        };

        self.map = function (mapper) {
            if (!_.isFunction(mapper)) {
                throw new TypeError("Mapper must be a function!");
            }

            if (self.isSuccessfulApiCall()) {
                var newObj = mapper(self);

                return createApiResultProperties(newObj, httpQueryResult);
            }

            return self;
        };

        self.toJSON = undefined;
        self.length = undefined;

        return self;
    }

    var self = createApiResultProperties(this, httpQueryResult);

    if (!self.isHttpError()) {
        _.extend(self, self.getHttpQueryResult().data);
        self.ErrorList = self.getHttpQueryResult().data.ErrorList;
    }

    self.__noSuchProperty__ = function (property) {
        log("WARNING!!!: access of non existing property '" + property +
            "' on api query result object" +
            "\napi query result is: " + JSON.stringify(self) +
            "\noriginal $http service call result: " + JSON.stringify(self.getHttpQueryResult()));
    }
}

function utkoClient() {
    return getOrCreateService('utkoClient', function (session, temp) {
        var $injector = $.injector;

        return new UtkoClient(session, temp, {
            apiUrl: $injector.apiUrl,

            MarketingPartnerKey: $injector.MarketingPartnerKey,

            requestIdProvider: function () {
                // return $.request.questionId
                return $jsapi.utils.uuid.random();
            },

            deviceIdProvider: function () {
                return $.request.channelUserId
            },

            createdStringProvider: function () {
                return (new Date()).toUTCString()
            },

            authHeader: $injector.authHeader,

            defaultHttpTimeout: $injector.defaultHttpTimeout,

            logging: {
                enabled: true,
                maxSize: 10,
                exampleSize: 10,
                pretty: true
            }
        });
    })
}