var utkonosErrors = {
    isCodeLimitExcessError: function (apiResult) {
        return apiResult.getApiErrorClass() === "phone_promo_exception_TimeLimitReached"
    }
};