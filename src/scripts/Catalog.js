function CatalogService(utkoClient, settings) {
    classCallCheck(this, CatalogService);

    var self = this;

    self.getCategories = function() {
        var result = utkoClient.call('?method=CatalogGetSections&PageSize=150');
        var data = result.getApiData();
        return httpQueryResultToList(data);
    };

    self.getItem = function(id) {
        var result = utkoClient.call('?method=CatalogGetItem&ID=' + id);
        var data = httpQueryResultToList(result.getApiData());
        return data[0];
    };

    self.log = createServiceLogger('CATALOG', settings.logging);
}


function catalog() {
    return getOrCreateService('catalog', function () {
        return new CatalogService(
            utkoClient(),
            {
                logging: {
                    enable: false,
                    pretty: true
                }
            }
        )
    });
}