describe('Memento', function () {
    it('should restore object', function () {
        function A (value) {
            this.field = value;
            this.method = function () {
                return this.field + "!";
            }
        }

        Memento.registerPojoObject(A, function (pojo) { return new A(pojo.field) });

        var a = new A(42);

        var serialized = Memento.makeMemento(a);

        var deserialized = Memento.restore(serialized);

        expect(deserialized.method()).toBe('42!');
    });

    it('should restore stateless function', function () {
        function add(a, b) {
            return a + b;
        }

        Memento.registerStatelessFunction(add);

        var m = Memento.makeMemento(add);

        expect(Memento.restore(m)).toBe(add);
        expect(Memento.restore(m)(1, 10)).toEqual(11);
    })
});