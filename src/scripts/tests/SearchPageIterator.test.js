function search() {
    return {
        goods: function (text, opts) {
            opts.Offset = opts.Offset || 0;
            return {
                GoodsItemList: goodsList.slice(opts.Offset, opts.Offset + opts.Count),
                TotalCount: goodsList.length + "",
                isSuccessfulApiCall: function () {
                    return true;
                }
            }
        }
    }
}

var goodsList = [1,2,3,4,5,6,7,8,9,10];

describe('SearchPageIterator', function () {
    it('should iterate forward', function () {
        var i = new SearchPageIterator('', {Count: 4});

        expect(i.isEmpty()).toBe(false);

        expect(i.hasNext()).toBe(true);
        expect(i.current().GoodsItemList).toEqual([1,2,3,4]);

        i.moveNext();
        expect(i.current().GoodsItemList).toEqual([5,6,7,8]);
        expect(i.hasNext()).toBe(true);

        i.moveNext();
        expect(i.current().GoodsItemList).toEqual([9,10]);
        expect(i.hasNext()).toBe(false);
    });
});