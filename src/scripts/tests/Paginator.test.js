describe('Paginator', function () {
    var paginator;
    var moreButton;

    beforeEach(function () {
        moreButton = false;
        paginator = new PaginatorSession(
            {},
            {},
            {
                logging: {
                    enabled: false,
                    pretty: true
                },
                moreButtonVisualiser: function () {
                    moreButton = true;
                },
                transition: function () {
                }
            }
        )
    });

    it('should paginate', function () {
        var digits = [];

        function TestProcessor() {
            this.process = function (page) {
                var lastDigit = page[0];
                digits.push(lastDigit);
            }
        }

        Memento.registerPojoObject(
            TestProcessor,
            function () { return new TestProcessor(); }
        );

        paginator.display({
            iterator: new ArrayPageIterator([1,2,3], 1),
            processor: new TestProcessor(),
            exitState: '/'
        });

        expect(digits).toEqual([1]);
        expect(moreButton).toBe(true);

        moreButton = false;
        paginator.nextPage();

        expect(digits).toEqual([1,2]);
        expect(moreButton).toBe(true);

        moreButton = false;
        paginator.nextPage();

        expect(digits).toEqual([1,2,3]);
        expect(moreButton).toBe(false);
    })

});