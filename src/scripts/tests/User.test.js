describe('User', function () {
   var user;
   var session;
   var apiResult;
   var lastSessionToken;

   beforeEach(function() {
       session = {};
       apiResult = {};
       lastSessionToken = undefined;

       user = new UserSession(session, {
           call: function () {
               apiResult.isSuccessfulApiCall = function () { return true };
               return apiResult;
           },
           getLastSessionToken: function () {
               return lastSessionToken;
           }
       });
   });

   it('should not be authenticated before authentication call', function () {
       expect(user.isAuthenticated()).toBe(false)
   });

   it('should save phone to session on authentication request', function () {
       var phone = '+79999999999';
       user.requestAuthenticationCode('+79999999999');
       expect(user.getPhone()).toBe(phone);
   });

   it('should detect authentication by last session token', function () {
       apiResult = {
           "UserSessionList": [
               {
                   "Created": "2018-03-16 02:52:31",
                   "AuthToken": "212158633f8e44fd993ec73695eb3eb6",
                   "Kpp": "92505095"
               }
           ]
       };

       lastSessionToken = "123";

       expect(user.isAuthenticated()).toBe(false);

       lastSessionToken = "456";

       user.authenticate();

       expect(user.isAuthenticated()).toBe(true);

       lastSessionToken = "789";

       expect(user.isAuthenticated()).toBe(false);
   })
});