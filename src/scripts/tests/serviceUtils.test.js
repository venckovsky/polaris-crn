describe('Service utils', function () {
    describe('longObjectsReplacer', function () {
        it('should replace long arrays', function () {
            var replacer = makeLongObjectsReplacer(5, null, 2);

            var result = replacer('key', [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);

            expect(result).toEqual([1, 2, "...8 more elements"]);
        });

        it('should replace long objects', function () {
            var replacer = makeLongObjectsReplacer(5, null, 2);

            var result = replacer('key', {a: 1, b: 2, c: 3, d: 4, f: 5, g: 6, h: 7});

            expect(result).toEqual({a: 1, b: 2, "...": "5 more elements"});
        });

        it('should use provided replacer', function () {
            var replacer = makeLongObjectsReplacer(100, function (key, value) {
                if (key !== 'Auth') {
                    return value;
                }
            });

            expect(JSON.stringify({a: 1, Auth: '1235'}, replacer)).toEqual('{"a":1}');
        });

        it('should replace long properties in JSON.stringify', function () {
            var replacer = makeLongObjectsReplacer(5, null, 5);

            var obj = [
                {
                    "_id": "5ab91aa700e35a84b355b473",
                    "index": 0,
                    "guid": "14b22d29-56a2-4f24-8fcb-afcc29ef5618",
                    "isActive": false,
                    "balance": "$1,485.07",
                    "picture": "http://placehold.it/32x32",
                    "age": 39,
                    "eyeColor": "brown",
                    "name": "Robinson Cardenas",
                    "gender": "male",
                    "company": "MAROPTIC",
                    "email": "robinsoncardenas@maroptic.com",
                    "phone": "+1 (948) 569-2238",
                    "address": "197 Furman Avenue, Machias, Indiana, 3016",
                    "about": "Qui ex ex minim ullamco duis. Non fugiat voluptate proident deserunt est. Esse sit fugiat pariatur eiusmod nisi consectetur ex mollit aliquip reprehenderit sunt cillum anim ex. Id ea velit in sunt mollit ipsum ex ea qui qui officia aute excepteur. Magna do culpa et ipsum nulla esse tempor dolore. Pariatur qui reprehenderit non ullamco pariatur anim culpa cupidatat cupidatat occaecat cillum eiusmod mollit enim. Consectetur enim sint esse ut proident exercitation veniam enim sit laboris consequat aliquip fugiat.\r\n",
                    "registered": "2014-03-08T06:19:17 -03:00",
                    "latitude": -15.154076,
                    "longitude": 171.500333,
                    "tags": [
                        "eiusmod",
                        "in",
                        "aliqua",
                        "fugiat",
                        "voluptate",
                        "dolore",
                        "qui"
                    ],
                    "friends": [
                        {
                            "id": 0,
                            "name": "Schmidt Everett"
                        },
                        {
                            "id": 1,
                            "name": "Mallory Maldonado"
                        },
                        {
                            "id": 2,
                            "name": "Bell Silva"
                        }
                    ],
                    "greeting": "Hello, Robinson Cardenas! You have 1 unread messages.",
                    "favoriteFruit": "banana"
                },
                {
                    "_id": "5ab91aa79b69e31ebc7c5ff8",
                    "index": 1,
                    "guid": "fb6ea951-aaa4-4f91-88bb-f5cf376868d9",
                    "isActive": false,
                    "balance": "$3,341.15",
                    "picture": "http://placehold.it/32x32",
                    "age": 20,
                    "eyeColor": "brown",
                    "name": "Ernestine Cunningham",
                    "gender": "female",
                    "company": "POSHOME",
                    "email": "ernestinecunningham@poshome.com",
                    "phone": "+1 (923) 496-3034",
                    "address": "481 Canal Avenue, Loyalhanna, Missouri, 3466",
                    "about": "Sit ipsum nisi deserunt commodo voluptate id mollit cupidatat labore enim. Incididunt eu fugiat quis sunt officia cillum minim eiusmod eiusmod nostrud. Pariatur amet est eu ad adipisicing. Ut ex consequat magna aliquip do et elit. Excepteur magna reprehenderit eu est cillum mollit enim ut nostrud excepteur. Officia dolore occaecat veniam qui et elit et.\r\n",
                    "registered": "2014-10-03T09:56:17 -03:00",
                    "latitude": 51.738246,
                    "longitude": -15.84005,
                    "tags": [
                        "laborum",
                        "minim",
                        "sunt",
                        "reprehenderit",
                        "ea",
                        "id",
                        "cupidatat"
                    ],
                    "friends": [
                        {
                            "id": 0,
                            "name": "Deanne Simpson"
                        },
                        {
                            "id": 1,
                            "name": "Shaffer Velasquez"
                        },
                        {
                            "id": 2,
                            "name": "Tessa Hahn"
                        }
                    ],
                    "greeting": "Hello, Ernestine Cunningham! You have 6 unread messages.",
                    "favoriteFruit": "strawberry"
                },
                {
                    "_id": "5ab91aa7f1e909c422b89dfe",
                    "index": 2,
                    "guid": "0af422d5-b84d-4862-9111-23fc843033d0",
                    "isActive": true,
                    "balance": "$3,956.59",
                    "picture": "http://placehold.it/32x32",
                    "age": 23,
                    "eyeColor": "brown",
                    "name": "Florence Lang",
                    "gender": "female",
                    "company": "TOURMANIA",
                    "email": "florencelang@tourmania.com",
                    "phone": "+1 (936) 459-2499",
                    "address": "690 Varanda Place, Richford, Iowa, 135",
                    "about": "Exercitation Lorem fugiat fugiat reprehenderit duis dolor sit aliquip deserunt qui aute esse ex id. Enim dolore elit exercitation quis minim sit officia sint aute adipisicing. Enim id aliqua proident tempor officia elit irure eiusmod enim mollit quis magna exercitation minim. Culpa anim incididunt exercitation in id ex nostrud Lorem duis veniam. Tempor velit duis proident consequat quis veniam exercitation. Proident esse eiusmod non Lorem.\r\n",
                    "registered": "2014-12-31T03:23:16 -03:00",
                    "latitude": 69.463383,
                    "longitude": 88.690306,
                    "tags": [
                        "deserunt",
                        "qui",
                        "dolore",
                        "nisi",
                        "sunt",
                        "ut",
                        "magna"
                    ],
                    "friends": [
                        {
                            "id": 0,
                            "name": "Arline Brady"
                        },
                        {
                            "id": 1,
                            "name": "Carson Carpenter"
                        },
                        {
                            "id": 2,
                            "name": "Goodman Jarvis"
                        }
                    ],
                    "greeting": "Hello, Florence Lang! You have 1 unread messages.",
                    "favoriteFruit": "banana"
                },
                {
                    "_id": "5ab91aa76da8e2995f745011",
                    "index": 3,
                    "guid": "df6ca3e3-d01f-4215-8e5b-e85ce5cefbb2",
                    "isActive": true,
                    "balance": "$1,113.19",
                    "picture": "http://placehold.it/32x32",
                    "age": 35,
                    "eyeColor": "green",
                    "name": "Hartman Conrad",
                    "gender": "male",
                    "company": "FROSNEX",
                    "email": "hartmanconrad@frosnex.com",
                    "phone": "+1 (902) 454-3349",
                    "address": "281 Seton Place, Rosewood, Maryland, 490",
                    "about": "Est nisi nostrud adipisicing dolore cupidatat ex reprehenderit et sunt laboris voluptate nisi. Officia tempor consequat Lorem pariatur tempor veniam ea ad voluptate cupidatat proident. Id dolore mollit est veniam irure deserunt nostrud eu. Lorem tempor incididunt mollit non magna eu qui aliqua officia eu irure voluptate mollit excepteur. Anim dolore non officia do sunt commodo ad et.\r\n",
                    "registered": "2015-07-14T08:16:20 -03:00",
                    "latitude": 35.94563,
                    "longitude": -168.865754,
                    "tags": [
                        "sunt",
                        "minim",
                        "ex",
                        "commodo",
                        "officia",
                        "deserunt",
                        "commodo"
                    ],
                    "friends": [
                        {
                            "id": 0,
                            "name": "Shana Molina"
                        },
                        {
                            "id": 1,
                            "name": "Dee Holland"
                        },
                        {
                            "id": 2,
                            "name": "Reed Boone"
                        }
                    ],
                    "greeting": "Hello, Hartman Conrad! You have 9 unread messages.",
                    "favoriteFruit": "strawberry"
                },
                {
                    "_id": "5ab91aa7ace66bd0ab7bdfeb",
                    "index": 4,
                    "guid": "d4881637-57c0-45bb-9433-879947fec1a6",
                    "isActive": true,
                    "balance": "$3,690.11",
                    "picture": "http://placehold.it/32x32",
                    "age": 27,
                    "eyeColor": "brown",
                    "name": "Maude Aguirre",
                    "gender": "female",
                    "company": "GENMEX",
                    "email": "maudeaguirre@genmex.com",
                    "phone": "+1 (804) 454-2919",
                    "address": "825 Dodworth Street, Salunga, Nevada, 2886",
                    "about": "In officia velit qui laborum reprehenderit duis. Tempor id ullamco laboris aliqua adipisicing minim. Elit pariatur laborum proident do laboris minim cillum ex aliqua cillum. Voluptate id dolor nostrud eu adipisicing excepteur consequat dolor. Et Lorem ea esse elit quis deserunt culpa et minim excepteur.\r\n",
                    "registered": "2014-12-07T09:58:58 -03:00",
                    "latitude": -77.130649,
                    "longitude": 60.753195,
                    "tags": [
                        "mollit",
                        "consequat",
                        "consequat",
                        "ex",
                        "sit",
                        "deserunt",
                        "est"
                    ],
                    "friends": [
                        {
                            "id": 0,
                            "name": "Buck Foster"
                        },
                        {
                            "id": 1,
                            "name": "Battle Doyle"
                        },
                        {
                            "id": 2,
                            "name": "Pennington Bryant"
                        }
                    ],
                    "greeting": "Hello, Maude Aguirre! You have 1 unread messages.",
                    "favoriteFruit": "apple"
                },
                {
                    "_id": "5ab91aa7104b0be0a95d97f2",
                    "index": 5,
                    "guid": "da1a5e97-04af-4b32-87b8-8b0e623f4371",
                    "isActive": false,
                    "balance": "$3,658.21",
                    "picture": "http://placehold.it/32x32",
                    "age": 24,
                    "eyeColor": "brown",
                    "name": "Edith Branch",
                    "gender": "female",
                    "company": "IMANT",
                    "email": "edithbranch@imant.com",
                    "phone": "+1 (994) 526-2347",
                    "address": "545 Robert Street, Siglerville, California, 449",
                    "about": "In tempor cillum occaecat aute eiusmod amet sit tempor. Ullamco eu do laboris mollit velit dolor ullamco fugiat. Qui esse exercitation duis quis veniam consectetur irure sint. Sit incididunt eu labore aliquip minim ad cupidatat ad nulla ex ipsum esse. Adipisicing laborum nisi nostrud ut consequat do esse. Sint cillum occaecat aute excepteur.\r\n",
                    "registered": "2018-02-16T08:47:36 -03:00",
                    "latitude": 6.737816,
                    "longitude": 43.334066,
                    "tags": [
                        "labore",
                        "laborum",
                        "esse",
                        "dolor",
                        "consequat",
                        "occaecat",
                        "excepteur"
                    ],
                    "friends": [
                        {
                            "id": 0,
                            "name": "Joy Riley"
                        },
                        {
                            "id": 1,
                            "name": "Letha Serrano"
                        },
                        {
                            "id": 2,
                            "name": "Deena Robles"
                        }
                    ],
                    "greeting": "Hello, Edith Branch! You have 2 unread messages.",
                    "favoriteFruit": "apple"
                }
            ];

            var expected = "[{\"_id\":\"5ab91aa700e35a84b355b473\",\"index\":0,\"guid\":\"14b22d29-56a2-4f24-8fcb-afcc29ef5618\",\"isActive\":false,\"balance\":\"$1,485.07\",\"...\":\"17 more elements\"},{\"_id\":\"5ab91aa79b69e31ebc7c5ff8\",\"index\":1,\"guid\":\"fb6ea951-aaa4-4f91-88bb-f5cf376868d9\",\"isActive\":false,\"balance\":\"$3,341.15\",\"...\":\"17 more elements\"},{\"_id\":\"5ab91aa7f1e909c422b89dfe\",\"index\":2,\"guid\":\"0af422d5-b84d-4862-9111-23fc843033d0\",\"isActive\":true,\"balance\":\"$3,956.59\",\"...\":\"17 more elements\"},{\"_id\":\"5ab91aa76da8e2995f745011\",\"index\":3,\"guid\":\"df6ca3e3-d01f-4215-8e5b-e85ce5cefbb2\",\"isActive\":true,\"balance\":\"$1,113.19\",\"...\":\"17 more elements\"},{\"_id\":\"5ab91aa7ace66bd0ab7bdfeb\",\"index\":4,\"guid\":\"d4881637-57c0-45bb-9433-879947fec1a6\",\"isActive\":true,\"balance\":\"$3,690.11\",\"...\":\"17 more elements\"},\"...1 more elements\"]" ;

            expect(JSON.stringify(obj, replacer)).toEqual(expected);
        });
    })
});