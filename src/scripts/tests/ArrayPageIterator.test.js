describe('ArrayPageIterator', function () {
    describe('iterations', function () {
        var iter;

        beforeEach(function () {
            iter = new ArrayPageIterator([1,2,3,4,5,6,7,8], 3);
        });

        function iterateForward() {
            expect(iter.isEmpty()).toBe(false);

            expect(iter.current()).toEqual([1,2,3]);
            expect(iter.hasNext()).toBe(true);

            iter.moveNext();
            expect(iter.current()).toEqual([4,5,6]);
            expect(iter.hasNext()).toBe(true);

            iter.moveNext();
            expect(iter.current()).toEqual([7,8]);
            expect(iter.hasNext()).toBe(false);
        }

        it('forward', iterateForward);

        function iterateForwardAndBackward() {
            iterateForward();

            expect(iter.hasPrevious()).toBe(true);

            iter.movePrevious();
            expect(iter.hasPrevious()).toBe(true);
            expect(iter.current()).toEqual([4, 5, 6]);

            iter.movePrevious();
            expect(iter.current()).toEqual([1, 2, 3]);
            expect(iter.hasPrevious()).toBe(false);
        }

        it('forward and then backward', iterateForwardAndBackward);

        it('forward and backward in a loop', function () {
            for (var i = 0; i < 10; i++) {
                iterateForwardAndBackward();
            }
        })
    });

    it('should handle empty arrays', function () {
        var iter = new ArrayPageIterator([], 1);

        expect(iter.isEmpty()).toBe(true);
        expect(iter.hasNext()).toBe(false);
        expect(iter.hasPrevious()).toBe(false);
        
        if (iter.isEmpty()) {
            return;
        } 
        
        do {
            process(iter.current());
        } while (iter.hasNext());
    })
});