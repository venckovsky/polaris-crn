describe('UtkoClient', function () {
    var client;

    beforeEach(function() {
        client = new UtkoClient({}, {}, {
            apiUrl: 'http://test.test',
            MarketingPartnerKey: '123',
            requestIdProvider: function () {
                return '456'
            },
            createdStringProvider: function () {
                return 'today'
            },
            deviceIdProvider: function () {
                return 'test';
            },
            logging: {
                enabled: true
            }
        });
        $global.$http = {
            query: function () {}
        };

    });

    function spySuccess() {
        spyOn($http, 'query').and.returnValue({
            isOk: true,
            status: 200,
            data: {
                Head: {
                    Status: 'success'
                },
                Body: {
                    A: 1,
                    B: 'B'
                }
            }
        });
    }

    function spyHttpError() {
        spyOn($http, 'query').and.returnValue({
            isOk: false,
            status: 500,
            error: 'Internal server error'
        });
    }

    function spyApiError(apiError) {
        spyOn($http, 'query').and.returnValue({
            isOk: true,
            status: 200,
            data: {
                Head: {
                    Status: 'fail'
                },
                Body: {
                    ErrorList: [
                        apiError
                    ]
                }
            }
        });
    }

    it('should call http.query with required params', function () {
        spySuccess();

        client.call({
            Method: 'userSessionAdd',
            Body: {
                Param: 'param'
            }
        });

        expect($http.query).toHaveBeenCalledWith(
            'http://test.test',
            {
                method: 'POST',
                dataType: "json",
                headers: {},
                body: {
                    Head: {
                        Method: 'userSessionAdd',
                        MarketingPartnerKey: '123',
                        Created: 'today',
                        RequestId: '456',
                        DeviceId: 'test'
                    },
                    Body: {Param: 'param'}
                }
            }
        );
    });

    it('should extract response body fields on success response', function () {
        spySuccess();

        var result = client.call({});

        expect(result.A).toEqual(1);
        expect(result.B).toEqual('B');
    });

    describe('api result', function () {
        it('should work with api errors', function () {
            var apiError = {
                "Class": "Utk\\Utkapi_Exception_BadUsernameOrPassword",
                "Data": [],
                "Code": "108",
                "Message": "Bad username or password",
                "Description": "Неверные имя пользователя или пароль",
                "Type": "4"
            };

            spyApiError(apiError);

            var result = client.call({});

            expect(result.isApiError()).toBe(true);
            expect(result.isHttpError()).toBe(false);

            expect(result.getApiStatus()).toEqual("fail");
            expect(result.getApiError()).toEqual(apiError);
            expect(result.getApiErrorCode()).toBe("108");
            expect(result.getApiErrorClass()).toBe("Utk\\Utkapi_Exception_BadUsernameOrPassword");
            expect(result.getApiErrorDescription()).toBe("Неверные имя пользователя или пароль");
            expect(result.getApiErrorString()).toBe("Bad username or password (Неверные имя пользователя или пароль)");
            expect(result.getHttpError()).toBe(undefined);
        });

        it('should work with http errors', function () {
            spyHttpError();

            var result = client.call({});

            expect(result.isHttpError()).toBe(true);
            expect(result.isApiError()).toBe(false);

            expect(result.getApiStatus()).toBe(undefined);
            expect(result.getApiError()).toBe(undefined);
            expect(result.getApiErrorCode()).toBe(undefined);
            expect(result.getApiErrorClass()).toBe(undefined);
            expect(result.getApiErrorDescription()).toBe(undefined);
            expect(result.getApiErrorString()).toBe(undefined);

            expect(result.getHttpError()).toBe('Internal server error');
        });

        it('should map on success', function () {
            spySuccess();

            var result = client.call({}).map(function (r) {
                return {A: r.A + 1}
            });

            expect(result.A).toBe(2);
            expect(result.B).toBe(undefined);
            expect(result.getApiStatus()).toBe('success');
        });

        it('should throw if mapper is not a function', function () {
            spySuccess();

            expect(function () {
                client.call({}).map(12)
            }).toThrowError(TypeError);
        });
    });

    describe('error handlers', function () {
        it('should be called on http errors', function () {
            spyHttpError();

            var error = "";

            client.addErrorHandler(function (r) {
                error = r.getHttpError();
            });

            client.call({});

            expect(error).toBe('Internal server error');
        });

        it('should be called on api errors', function () {
            spyApiError({Code: '42'});

            var code = "";

            client.addErrorHandler(function (r) {
                if (r.isApiError()) {
                    code = r.getApiErrorCode();
                }
            }, true);

            client.call({});

            expect(code).toBe('42');
        })
    });


});