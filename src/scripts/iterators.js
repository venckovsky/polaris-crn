function ArrayPageIterator(arr, pageSize, offset) {
    classCallCheck(this, ArrayPageIterator);

    this.arr = arr;
    this.pageSize = pageSize || 5;
    this.offset = offset || 0;

    if (!_.isArray(this.arr)) {
        throw new TypeError("arr must be an array");
    }
    if (!_.isNumber(this.pageSize)) {
        throw new TypeError("pageSize must be a number");
    }
    if (!_.isNumber(this.offset)) {
        throw new TypeError("offset must be a number");
    }

    this.isEmpty = function () {
        return this.arr.length === 0;
    };

    this.hasNext = function () {
        return this.offset < this.arr.length - this.pageSize;
    };

    this.hasPrevious = function () {
        return this.offset >= pageSize;
    };

    this.current = function () {
        return this.arr.slice(this.offset, this.offset + this.pageSize);
    };

    this.moveNext = function () {
        this.offset += this.pageSize;
    };

    this.movePrevious = function () {
        this.offset -= this.pageSize;
    };
}

Memento.registerPojoObject(
    ArrayPageIterator,
    function (pojo) {
        return new ArrayPageIterator(pojo.arr, pojo.pageSize, pojo.offset)
    }
);

function SearchPageIterator(text, opts) {
    classCallCheck(this, SearchPageIterator);
    var self = this;

    self.text = text;
    self.pageSize = 5;
    self.opts = opts;
    self.opts.Offset = self.opts.Offset || 0;

    self.isEmpty = function () {
        return false;
    };

    self.hasNext = function () {
        if (!self.resultWasSuceessfull) {
            return true;
        }
        return self.opts.Offset < self.result.GoodsItemList.length - self.pageSize;
    };

    self.hasPrevious = function () {
        if (!self.resultWasSuceessfull) {
            return true;
        }

        return self.opts.Offset >= self.result.GoodsItemList.length;
    };

    self.current = function () {
        self.result = search().goods(self.text, self.opts);
        self.resultWasSuceessfull = self.result.isSuccessfulApiCall();

        return self.result.GoodsItemList.slice(self.opts.Offset, self.opts.Offset + self.pageSize);
    };

    self.moveNext = function () {
        if (self.resultWasSuceessfull) {
            self.opts.Offset += self.pageSize;
        }
    };

    self.movePrevious = function () {
        if (self.resultWasSuceessfull) {
            self.opts.Offset -= self.pageSize;
            if (self.opts.Offset < 0) {
                self.opts.Offset = 0;
            }
        }
    };
}

Memento.registerPojoObject(
    SearchPageIterator,
    function (pojo) {
        var iterator = new SearchPageIterator(pojo.text, pojo.opts);
        iterator.resultWasSuceessfull = pojo.resultWasSuceessfull;
        iterator.result = pojo.result;
        return iterator;
    }
);