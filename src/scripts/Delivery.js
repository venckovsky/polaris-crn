function DeliveryService(utkoClient, settings) {
    classCallCheck(this, DeliveryService);

    var self = this;


    // информация о доставке
    self.deliveryInfo = function () {
        var result = utkoClient.call({
            Method: 'deliveryInfo'
        });

        return result;
    }; 

    self.getFoodboxTerminals = function () {
        var result = utkoClient.call({
            Method: 'foodboxTerminalSearch',
            Body: {
                SeparateFoodboxErrors: true
                //AddPickpointTerminals: true
            }
        });

        return result;
    };     

    self.getUserAddresses = function () {
        var result = utkoClient.call({
            Method: 'userAddressSearch'
        });

        return result;
    };  

    self.ifInDeliveryZone = function (lat, lon) {
        var result = utkoClient.call({
            Method: 'userAddressCheck',
            Body: {
                Latitude: lat,
                Longitude: lon
            }
        });

        return result;
    };      

    self.add = function (flatNumber, houseNumber, streetTitle, info, lat, lon) {
        var result = utkoClient.call({
            Method: 'userAddressAdd',
            Body: {
                UserAddress: {
                    FlatNumber: flatNumber,
                    HouseNumber: houseNumber,
                    StreetTitle: streetTitle,
                    Information: info,
                    YandexLatitude: lat,
                    YandexLongitude: lon
                }
            }
        });

        return result;
    };

    self.modify = function (modifiedAddress) {
        var result = utkoClient.call({
            Method: 'userAddressModify',
            Body: {
                UserAddress: modifiedAddress
            }
        });

        return result;
    };

    self.delete = function (userAddressId) {
        var result = utkoClient.call({
            Method: 'userAddressDelete',
            Body: {
                Id: userAddressId
            }
        });

        return result;
    };


    self.search = function (offset, count) {
        var result = utkoClient.call({
            Method: 'userAddressSearch',
            Body: {
                Offset: offset,
                Count: count
            }
        });

        return result;
    };

    self.addInterval = function (id) {
        var result = utkoClient.call({
            Method: 'cartModify',
            Body: {
                DeliveryIntervalId: id
            }
        });

        return result;
    };

    self.log = createServiceLogger("DELIVERY SERVICE", settings.logging);
}

function delivery() {
    return getOrCreateService('delivery', function () {
        return new DeliveryService(
            utkoClient(),
            {
                logging: {
                    enable: false,
                    pretty: true
                }
            }
        )
    });
}
