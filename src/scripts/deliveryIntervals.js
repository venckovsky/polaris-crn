function DeliveryIntervalsService(utkoClient, session, settings) {
    classCallCheck(this, DeliveryIntervalsService);

    var self = this;

    self.getUserAddressDeliveryIntervals = function (userAddressId) {
        var result = utkoClient.call({
            Method: 'userAddressDeliveryIntervalSearch',
            Body: {
                UserAddressId: userAddressId,
                Return: {
                    Disabled: true
                }
            }
        });

        if (result.isPending()) {
            session.deliveryPendingKey = result.getPendingRequestKey();
        }

        return result;
    }; 


    self.getPendingIntervalsDelivery = function() {
        if (session.deliveryPendingKey) {
            var result = utkoClient.getPending(session.deliveryPendingKey);

            if (result.isSuccessfulApiCall() && !result.isPending()) {
                session.deliveryPendingKey = undefined;
            }

            return result;
        }
    }

    self.havePendingDeliveryInterval = function() {
        return !!session.deliveryPendingKey;
    }

    self.getFoodboxDeliveryIntervals = function (terminalId) {
        var result = utkoClient.call({
            Method: 'foodboxIntervalSearch',
            Body: {
                TerminalId: terminalId
            }
        });

        if (result.isPending()) {
            session.deliveryPendingKey = result.getPendingRequestKey();
        }

        return result;
    };      

    self.log = createServiceLogger("DELIVERY INTERVALS SERVICE", settings.logging);
}

function deliveryIntervals() {
    return getOrCreateService('deliveryIntervals', function (session) {
        return new DeliveryIntervalsService(
            utkoClient(),
            session,
            {
                logging: {
                    enable: false,
                    pretty: true
                }
            }
        )
    });
}