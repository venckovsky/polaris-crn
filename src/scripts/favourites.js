function Favourites(utkoClient, settings) {
    classCallCheck(this, Favourites);

    var self = this;
    var token = "74tziBKK-HYzf7ETh";

    self.getFavouritesList = function() {
        var result = utkoClient.call('?method=FavGetList&token=' + token);
        var data = result.getApiData();
        return httpQueryResultToList(data);
    };

    self.favAdd = function(id) {
        var result = utkoClient.call('?method=FavAdd&ID=' + id + '&token=' + token);
        var data = result.getApiData();
        return httpQueryResultToList(data);
    };
    self.favDel = function(id) {
        var result = utkoClient.call('?method=FavDell&ID=' + id + '&token=' + token);
        var data = result.getApiData();
        return httpQueryResultToList(data);
    };

    self.log = createServiceLogger('FAVOURITES', settings.logging);

}

function favourites() {
    return getOrCreateService('favourites', function () {
        return new Favourites(
            utkoClient(),
            {
                logging: {
                    enable: false,
                    pretty: true
                }
            }
        )
    });
}