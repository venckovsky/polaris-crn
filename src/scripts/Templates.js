function templates(utkoClient, settings) {
    classCallCheck(this, templates);

    var self = this;
//    var token = session.token;
    var token = '74tziBKK-HYzf7ETh';

    self.getTemplatesList = function() {
        var result = utkoClient.call('?method=OTemplateGetList&token=' + token);
        var data = result.getApiData();
        result.TemplatesList = httpQueryResultToList(data);
        $jsapi.context().session.TemplatesList = data;
        return result;
    };

    self.addTemplate = function(id, name) {
        // из меню "мои заказы" при просмотре заказа > добавить шаблон заказа
        var result = utkoClient.call('?method=OTemplateAdd&token=' + token + '&ID=' + id + '&NAME=' + encodeURIComponent(name)); 
        return result;
    };

    self.removeTemplate = function(id) {
        var result = utkoClient.call('?method=OTemplateDell&token=' + token + '&ID=' + id);
        return result;
    };

    self.makeOrderWithTemplateId = function(id) {
        var result = utkoClient.call('?method=OTemplateOrder&ID=' + id + '&token=' + token);
        return result;
    };

    self.orderTemplateGet = function(id) {
        var result = utkoClient.call('?method=OTemplateGet&token=' + token + '&ID=' + id);
        return result;
    };

    self.log = createServiceLogger('ORDERTEMPLATES', settings.logging);

}

function getTemplateIdByName (name, processor) {
        return _.findWhere(processor, {ORDER_TEMPLATE_NAME: name});
    }

function OrderTemplates() {
    return getOrCreateService('ordertemplates', function () {
        return new templates(
            utkoClient(),
            {
                logging: {
                    enable: false,
                    pretty: true
                }
            }
        )
    });
}