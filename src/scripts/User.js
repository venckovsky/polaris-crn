function UserSession(session, utkoClient) {
    classCallCheck(this, UserSession);

    var self = this;

    self.isAuthenticated = function () {
        //return !!(session.sessionToken && (session.sessionToken === utkoClient.getLastSessionToken()));
        return true;
    };

    self.isRegistered = function () {
        return !!session.isRegistered;
    };

    self.getPhone = function () {
        return session.phone;
    };

    self.getEmail = function() {
        return session.email;
    };

    self.requestAuthenticationCode = function (phone) {
        session.phone = phone;

        var result = utkoClient.call('?method=Auth&phone=' + phone);

        if (result.isSuccessfulApiCall()) {
            session.isRegistered = result.Registered;
        }

        return result;
    };

    self.requestAuthenticationCodebyEmail = function (email) {
        session.email = email;

        var result = utkoClient.call('?method=Auth&email=' + email);

        if (result.isSuccessfulApiCall()) {
            session.isRegistered = result.Registered;
        }

        return result;
    };

    self.authenticate = function (code, login) {
        var username = login || session.phone;
        var password = code;
        var result = utkoClient.call('?method=Auth&phone='+ username + '&code=' + code);

        if (result.isSuccessfulApiCall()) {
            session.userSessionList = result.UserSessionList;
            session.sessionToken = utkoClient.getLastSessionToken();
        }

        return result;
    };

    self.deauthenticate = function () {
        if (self.isAuthenticated()) {
            utkoClient.call('?method=AuthLogout&token=' + session.token);
            }
        }
    }


function user() {
    return getOrCreateService('user', function (session) {
        return new UserSession(session, utkoClient())
    });
}