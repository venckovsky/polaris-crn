function OrderService(session, temp, utkoClient, settings) {
    classCallCheck(this, OrderService);

    var self = this;
    var token = '74tziBKK-HYzf7ETh';

    self.add = function (name, phone, street, house, flat) {
        var query = '?method=OrderAdd&token=' + token + '&NAME=' + encodeURIComponent(name)
                    + '&PERSONAL_PHONE=' + phone + '&STREET=' + encodeURIComponent(street) 
                    + '&HOUSE=' + house + '&APARTMENT=' + flat;
        var result = utkoClient.call(query);
        var $session = $jsapi.context().session;
        var data = result.getApiData();
        $session.currentOrderId = data.ID;
        return result;
    }; 

    self.cancel = function (id) {
        var result = utkoClient.call('?method=OrderCancel&token=' + token + '&ID=' + id);
        return result;
    };

    self.usePromocode = function(promocode) {
        var result = utkoClient.call('?method=OrderSetPromocode&token=' + token + '&PROMOCODE=' + promocode);
        return result;
    };

    self.deactivatePromocode = function() {
        var result = utkoClient.call('?method=OrderDellPromocode&token=' + token);
        return result;
    };

    self.search = function(id) {
        var result = utkoClient.call('?method=OrderGetList&token=' + token);
        var data = result.getApiData();
        result.OrderList = httpQueryResultToList(data);
        return result;
    };

    self.chooseDeliveryType = function(id) {
        var result = utkoClient.call('?method=OrderSetDelivery&token=' + token + '&ID=' + id);
        return result;
    };

    self.getOrder = function(id) {
        var result = utkoClient.call('?method=OrderGet&token=' + token + '&ID=' + id);
        return result.getApiData();
    };

    self.log = createServiceLogger("ORDER SERVICE", settings.logging);
}

function order() {
    return getOrCreateService('order', function (session, temp) {
        return new OrderService(
            session, 
            temp,
            utkoClient(),
            {
                logging: {
                    enable: false,
                    pretty: true
                }
            }
        );
    });
}
