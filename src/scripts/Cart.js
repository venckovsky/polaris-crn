function CartSession(session, temp, utkoClient, settings) {
    classCallCheck(this, CartSession);

    var self = this;
    var token = "74tziBKK-HYzf7ETh";;

    var cartReturnAll = {
        NAME: true,
        PRICE: true,
        QUANTITY: true,
    };
    
    self.add = function (itemId, quantity) {
        cartUpdated();
        var result = utkoClient.call('?method=BasketAdd&ID=' + itemId + '&token=' + token);
        var data = result.getApiData();
        return httpQueryResultToList(data);
    };

    self.delete = function (itemId) {
        cartUpdated();
        var result = utkoClient.call('?method=BasketDell&ID=' + itemId + '&token=' + token);
        var data = result.getApiData();
        return httpQueryResultToList(data);
    };

    self.getItemById = function(itemId) {
        var cart = self.lookup();
        var currentItem = {};
        currentItem = _.findWhere(cart, {PRODUCT_ID: itemId});
        return currentItem;
    };

    self.clear = function () {
        cartUpdated();
        var result = utkoClient.call('?method=BasketClean&token=' + token);
        var data = result.getApiData();
        return httpQueryResultToList(data);
    };

    self.lookup = function () {
        cartUpdated();
        var result = utkoClient.call('?method=BasketGet&token=' + token);
        var data = result.getApiData();
        return httpQueryResultToList(data);
    };

    self.isEmpty = function () {
        var cart = self.lookup();

        if (cart.isSuccessfulApiCall()) {
            return true;
        } 
    };

    function cartUpdated() {
        temp.cart = undefined;
        temp.cartSuccess = undefined;
    }

    function shouldInvalidateCurrentCart() {
        return !temp.cart || !temp.cartSuccess;
    }

    function saveCurrentCart(cart) {
        temp.cart = cart;
        temp.cartSuccess = cart.isSuccessfulApiCall();
    }

    function getCurrentCart() {
        return temp.cart;
    }

    function logApiResult(msg, result) {
        self.log(msg, result.isSuccessfulApiCall() && result || result.getApiError());
        return result;
    }

    this.log = createServiceLogger('CART', settings.logging);
}

function cart() {
    return getOrCreateService('cart', function (session, temp) {
        return new CartSession(
            session,
            temp,
            utkoClient(),
            {
                logging: {
                    enabled: false
                }
            }
        )
    });
}