var Memento = (function () {
    var registry = [];

    return {
        register: function (fromPOJO, toPOJO) {
            if (!_.isFunction(fromPOJO)) {
                throw new TypeError("fromPOJO must be a function");
            }

            if (toPOJO && !_.isFunction(toPOJO)) {
                throw new TypeError("toPOJO must be a function");
            }

            registry.push({
                toPOJO: toPOJO,
                fromPOJO: fromPOJO
            });

            return registry.length - 1;
        },

        registerPojo: function (fromPOJO) {
            return Memento.register(fromPOJO, _.identity);
        },

        makeMemento: function (obj, id) {
            id = _.isUndefined(obj.__mementoId) ? id : obj.__mementoId;

            if (!registry[id]) {
                throw new Error("Can't convert to POJO. Id '" + id + "' is not registered");
            }

            return {
                id: id,
                pojo: registry[id].toPOJO && registry[id].toPOJO(obj)
            };
        },

        restore: function (memento) {
            return registry[memento.id].fromPOJO(memento.pojo)
        },

        registerObject: function (Constructor, fromPOJO, toPOJO) {
            Constructor.prototype.__mementoId = Memento.register(fromPOJO, toPOJO)
        },

        registerPojoObject: function (Constructor, fromPOJO) {
            Memento.registerObject(Constructor, fromPOJO, _.identity)
        },

        getId: function (obj) {
            if (_.isUndefined(obj.__mementoId)) {
                throw new Error("Object is not registered in with Memento");
            }

            return obj.__mementoId;
        },

        saveId: function (obj, id) {
            obj.__mementoId = id;
        },

        registerStatelessFunction: function (func) {
            func.__mementoId = Memento.register(function () {
                return func;
            });
        }
    }
})();