/////////////////// CART ///////////////////

function GoodsPageProcessor(buttons, items, categoryId) {
    var self = this;

    self.items = items || {};
    self.buttons = buttons;
    self.categoryId = categoryId;

    var emptyGoodsItemListAnswer = 'Сейчас в этой категории товаров нет. Давайте поищем что-нибудь ещё.';

    self.process = function (page) {
        page.forEach(function (item) {
            self.items[item.NAME] = item;
        });
        if (page.length === 1) {
            $.session.currentItem = page[0];
            visualiseGoodsDetails(page[0]);
            $reactions.transition({value: '/Catalog/Search/VisualiseItem', deferred: true});
        } else if (page.length > 1) {
            visualiseGoodsPage(page);
        } else {
            $reactions.transition('/Catalog/Search/DoSearch/NoResults');
        }
        
        if (self.buttons) {
            $reactions.buttons(self.buttons);
        }
    };
}


Memento.registerPojoObject(
    GoodsPageProcessor,
    function (pojo) {
        return new GoodsPageProcessor(pojo.buttons, pojo.items, pojo.categoryId);
    }
);


function CategoriesPageProcessor(items, categoryId) {
    var self = this;

    self.items = items || {};
    self.categoryId = categoryId;

    self.process = function (page) {
        visualiseCategoriesPage(page);

        page.forEach(function (category) {
            self.items[category] = category;
        });
    };
}

Memento.registerPojoObject(
    CategoriesPageProcessor,
    function (pojo) {
        return new CategoriesPageProcessor(pojo.items, pojo.categoryId);
    }
);


function modifyQuantity(currentItem, mode, newQuantity) {
    var $session = $jsapi.context().session;
    var QuantityNew;
    var itemId = currentItem.PRODUCT_ID;
    var inStockCount = parseInt(currentItem.InStockCount);             
    var itemQuantity = parseInt(currentItem.QUANTITY) || 1; 
    var newQuantityCase = false;           
    var noMoreItemsText = 'К сожалению, количество этого товара ограничено.';
    var changeQuantityText = 'Вы изменили количество товара.';
    switch (mode) {
    case 'add':
            cart().add(itemId);
            $reactions.answer(changeQuantityText);  
        break;
    case 'subtract':
        if (itemQuantity - newQuantity < 1) {
            $reactions.transition('/Cart/Modify/ModifyQuantity/IfDelete');
        } else {
            QuantityNew = itemQuantity - newQuantity; 
            $reactions.answer(changeQuantityText); 
            $session.newQuantity = QuantityNew;
        }
        break;
    case 'newQuantity':
        if (newQuantity > inStockCount) {
            $reactions.answer(noMoreItemsText);
            cart().add(itemId, inStockCount - itemQuantity);
        } else {
            QuantityNew = itemQuantity + newQuantity; 
            $reactions.answer(changeQuantityText);
            $session.newQuantity = QuantityNew; 
        }
        break;
    }
}

function checkOrder() {
    $.session.deliveryInfo = $.session.deliveryInfo || delivery().deliveryInfo();

    var minCostFoodbox = $.session.deliveryInfo.MinCostDeliveryForFoodbox;
    var minCostAddress = $.session.deliveryInfo.MinOrder;
    var maxWeight = $.session.deliveryInfo.Weight;
    var maxCost = 10000; // максимальная сумма заказа без предоплаты

    var cartLookup = cart().lookup();

    return {type: '', answer: ''};
}

function navigateCatalog() {
    var iterator = paginator().getIterator();
    if (iterator && iterator.hasPrevious()) {
        paginator().previousPage();
    } else {
        if (paginator().canRestore()) {
            paginator().restore();
        } else {
            paginator().currentPage();
        }                  
    }
}



/////////////////// DELIVERY ///////////////////


function TerminalsPageProcessor(buttons, items) {
    var self = this;

    self.items = items || {};
    self.buttons = buttons;

    self.process = function (page) {
        visualiseTerminalsPage(page);
        
        if (self.buttons) {
            $reactions.buttons(self.buttons);
        }
    };
}

Memento.registerPojoObject(
    TerminalsPageProcessor,
    function (pojo) {
        return new TerminalsPageProcessor(pojo.buttons, pojo.items);
    }
);

function IntervalsPageProcessor(buttons, items) {
    var self = this;

    self.items = items || {};
    self.buttons = buttons;


    self.process = function (page) {
        visualiseIntervalsPage(page);
        
        if (self.buttons) {
            $reactions.buttons(self.buttons);
        }
    };
}

Memento.registerPojoObject(
    IntervalsPageProcessor,
    function (pojo) {
        return new IntervalsPageProcessor(pojo.buttons, pojo.items);
    }
);

function getUserAddressInfoDict(userAddressList) {
    var addressInfo = {};
    var text;
    var mark;
    
    userAddressList.forEach(function (item) {
        text = '';

        if (item.Information === 'Дом' || item.Information === 'Работа' || item.Information === 'Дача') {
            mark = item.Information;
            text += mark + '-';
        }

        if (item.TownTitle !== 'Москва') {
            text += item.TownTitle + ', ';
        }
        text += item.StreetTitle + ', д. ' + item.HouseNumber;
        if (item.FlatNumber !== '-') {
            text += ', кв. ' + item.FlatNumber;
        }

        addressInfo[text] = {id: item.Id};
        
        if (mark) {
            addressInfo[text].mark = mark;
        }

        if (item.Editable) {
            addressInfo[text].editable = true;
        } else if (!item.Editable){
            addressInfo[text].editable = false;
        }

    });
    return addressInfo;    
}

function getMarkedAddress(mark, addressInfo) {
    var addresses = Object.keys(addressInfo);
    for (var i = 0; i < addresses.length; i++) {
        if (addressInfo[addresses[i]].mark === mark) {
            return addresses[i];
        }
    }
    return;
}

function updateUserAddressList() {
    var userAddressSearch = delivery().search(0, 5);
    if (!userAddressSearch.isTimeout()) {
        $.session.addressInfo = getUserAddressInfoDict(userAddressSearch.UserAddressList);    
    }
}


cnv.houseNumberConverter = function(parseTree) {
    var value = '';

    if (parseTree.Number && parseTree.Number[0]) {
        value += parseTree.Number[0].value;
    }

    if (parseTree.extraHouseInfo && parseTree.extraHouseInfo[0]) {
        value += ' ' + parseTree.extraHouseInfo[0].text;
    }

    if (parseTree.AnyLetter && parseTree.AnyLetter[0]) {
        value += ' ' + parseTree.AnyLetter[0].text;
    } else if (parseTree.Number && parseTree.Number[1]) {
        value += ' ' + parseTree.Number[1].value;
    }

    return value;
}


/////////////////// AUTHORISATION ///////////////////

function processCodeRequestResult (result) {
    var $temp = $.temp;
    if (result.isSuccessfulApiCall()) {
        if($temp.lostCode){
            $reactions.answer('Выслан новый код.');
        }
        $reactions.transition('/Authorisation/AskCode');
    } else if (result.isHttpError()) {
        $reactions.answer('Что-то не так с соединением. Попробуйте запросить код ещё раз.');
        $reactions.buttons([{text: 'Запросить код ещё раз', hide:true, transition: '/Authorisation/GetCode'}]);  
    } else if (utkonosErrors.isCodeLimitExcessError(result)){
        $reactions.answer('Превышено количество запросов кода. Повторите попытку авторизоваться позже.');
    } else if (result.getApiErrorDescription()) {
        $reactions.answer(result.getApiErrorDescription());
    } else {
        $reactions.answer('Что-то пошло не так. Я пока не пойму что, но мои разработчики обязательно разберутся с этим.');
    }
}

/////////////////// ORDER ///////////////////

function OrdersPageProcessor(buttons, items) {
    var self = this;

    self.items = items || {};
    self.buttons = buttons;

    self.process = function (page) {
        visualiseOrdersPage(page);
        
        if (self.buttons) {
            $reactions.buttons(self.buttons);
        }
    };
}

Memento.registerPojoObject(
    OrdersPageProcessor,
    function (pojo) {
        return new OrdersPageProcessor(pojo.buttons, pojo.items);
    }
);


function modifyOrderCart(orderId, currentItem, mode, newQuantity) {
    var itemQuantity = parseInt(currentItem.Quantity);
    var updatedQuantity;
    var goodsList;
    var newItem;
    var updatedOrder;


    switch (mode) {    
    case 'add':
        updatedQuantity = itemQuantity + newQuantity;
        updatedOrder = order().modify(orderId, currentItem.Id, updatedQuantity);
        break;
    case 'subtract':
        updatedQuantity = itemQuantity - newQuantity;
        if (updatedQuantity < 1) {
            $reactions.transition('/Order/OrderModify/ModifyQuantity/IfDelete');
        } else {
            updatedOrder = order().modify(orderId, currentItem.Id, updatedQuantity);
        }
        break;
    case 'newQuantity':
        updatedQuantity = newQuantity;
        updatedOrder = order().modify(orderId, currentItem.Id, updatedQuantity);
        break;
    }

    if (updatedOrder && updatedOrder.OrderList && updatedOrder.OrderList.OrderItemList) {
        goodsList = updatedOrder.OrderList.OrderItemList;
        newItem = getNewCurrentItem(goodsList, currentItem);
        if (newItem.Quantity === currentItem.Quantity) {
            if (updatedQuantity > currentItem.Quantity) {
                $reactions.answer('К сожалению, количество товара ограничено.');
            } else {      
                $reactions.answer('К сожалению, такое количество товара недопустимо.');
            }
        } else {
            $reactions.answer('Ваш заказ изменён.');
            $.session.currentItem = newItem;
            $reactions.transition('/Order/OrderModify/VisualiseItemModifyInfo');
        }
    } else {
        if (updatedOrder && updatedOrder.ErrorList && updatedOrder.ErrorList[0].Message) {
            $reactions.answer(updatedOrder.ErrorList[0].Message);
        } else {
            $reactions.answer('К сожалению, у меня не получилось изменить заказ.');
            $reactions.transition('/Order/OrderHistory/OrderLookup');        
        }

    }

    
}

function getNewCurrentItem(goodsList, currentItem) {
    var id = currentItem.Id;

    for (var i = 0; i < goodsList.length; i++) {
        if (goodsList[i].Id === id) {
            return goodsList[i];
        }
    }

    return currentItem;
}

/////////////////// FAVOURITES ///////////////////

function FavouritesPageProcessor(buttons, items) {
    var self = this;
    self.items = items || {};
    self.buttons = buttons;

    self.process = function (page) {
        visualiseFavouritesPage(page);

        if (self.buttons) {
            $reactions.buttons(self.buttons);
        }

    };
}

Memento.registerPojoObject(
    FavouritesPageProcessor,
    function (pojo) {
        return new FavouritesPageProcessor(pojo.buttons, pojo.items);
    }
);

/////////////////// TEMPLATES ///////////////////

function TemplatesPageProcessor(items, categoryId) {
    var self = this;

    self.items = items || {};
    self.categoryId = categoryId;

    self.process = function (page) {
        visualiseTemplatesPage(page);

        page.forEach(function (name) {
            self.items[name] = name;
        });
    };
}

Memento.registerPojoObject(
    TemplatesPageProcessor,
    function (pojo) {
        return new TemplatesPageProcessor(pojo.items, pojo.categoryId);
    }
);