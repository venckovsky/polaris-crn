var DeliveryUtils = (function() {

    function checkOrderPrice() {
        var $session = $jsapi.context().session, $temp = $jsapi.context().temp, $client = $jsapi.context().client, $response = $jsapi.context().response;
        var order_price = getTotalSum();
        $session.take.deliveryMinPrice = parseInt($session.take.deliveryMinPrice);

        if ($session.take.deliveryMinPrice >= order_price) {
            $reactions.answer("Ваш заказ: " + order_price + " руб.\nМинимальная стоимость заказа – " + $session.take.deliveryMinPrice + " руб.\nВы можете поменять доставку на самовывоз или добрать товаров.")
            $temp.nextState = "/Delivery/MenuAfterMinPrice"
        } else {
            $session.take.statusDelivery = 0;
            $temp.nextState = "/ConfirmOrder"
        }
    }

    function formatDeliveryPrice(data){
        var $session = $jsapi.context().session;
        var $temp = $jsapi.context().temp;
        if (data)
        {
            $session.take.avaliable = data.deliveryAvailable == 1 ? true : false;
            if ($session.take.avaliable == 0) {
                if ($session.timeType == "asap"){
                    $reactions.answer('Доставка в ближайшее время невозможна. Пожалуйста, выберите другое время.');
                } else {
                    $reactions.answer('В указанное время доставка невозможна. Пожалуйста, выберите другое время.');
                }
                $temp.nextState = "/Delivery/TimeHandler";
                $session.take.statusTime = "no delivery";
        
            } else {
                $temp.nextState = "/Delivery/OrderPriceHandler"
                $session.take.statusTime = "OK"
            }
            $session.take.deliveryPrice = data.deliveryPrice;
            $session.take.deliveryMinPrice = data.deliveryPriceValueMix[0].min_sum;
        }
        else {
            $temp.nextState = "/Delivery";
        }
    }

    function loadDeliveryPrice(){
        var $session = $jsapi.context().session;
        var $temp = $jsapi.context().temp;
        $temp.order_price = getTotalSum();
        var restaurantNotWorkingString = "У меня не получается узнать, может ли ресторан сделать заказ. Пожалуйста, попробуйте ещё раз.";
        if ($session.timeType == "asap"){
            checkAvaliableAndDeliverySumNoTime($session.take.restaurant_id, $temp.order_price).then(function (data) {
                $temp.DeliveryPriceInfo = data;
            }).catch(function (err){
                handleNotWorkingRest();
            });
        } 
        else {
            checkAvaliableAndDeliverySum($session.take.restaurant_id, $temp.order_price, $session.orderWanted).then(function (data){
                $temp.DeliveryPriceInfo = data;
            }).catch(function (err){
                handleNotWorkingRest();
            });
        }      
    }

    function handleNotWorkingRest(){
        $reactions.answer("У меня не получается узнать, может ли ресторан сделать заказ. Пожалуйста, попробуйте ещё раз.");
        $temp.nextState = "/Delivery";
    }

    function fillIntervalsDct(intervalsRes) {
        var $session = $jsapi.context().session;
        if ($session.deliveryOptions.deliveryType === 'foodbox') {
            $session.intervals = intervalsRes.IntervalList;
        } else {
            $session.intervals = intervalsRes.DeliveryIntervalList;
        }
        if ($session.intervals && $session.intervals.length > 0) {
            $session.intervalsDct = {};
            $session.intervals.forEach(function(item) {
                //item.Title = formatIntervalTitle(item.Title, item.Date);
                $session.intervalsDct[item.Title] = {'Id': item.Id, 'EditableUntil': item.EditableUntil};
            });
        }
    }

    //функция ищет подходящие по времени интервалы среди всех доступных для адреса интервалов
    function intervalTimeSearch(userTime, intervals) {
        var proposedIntervals = [];
        var newProposedIntervals = [];

        //если пользователь указал ближайшее время, а потом решил поменять интервал
        if (!userTime) {
            return [];
        } else {
            var isHour = false;
            if (userTime.value && userTime.value.hour !== undefined) {
                isHour = true;
            }
            var date = toMoment(userTime).locale('ru').format('YYYY-MM-DD');
            for (var i = 0; i < intervals.length; i++) {
                if (intervals[i].Date === date) {
                    proposedIntervals.push(intervals[i])
                }
            }
            if (proposedIntervals.length > 0) {
                if (isHour) {
                    var time = toMoment(userTime).locale('ru').format('HH:mm:ss');
                    var userTimeMoment = moment(time);
                    for (var i = 0; i < proposedIntervals.length; i++) {
                        var timeFromMoment = moment(proposedIntervals[i].TimeFrom);
                        var timeToMoment = moment(proposedIntervals[i].TimeTo);
                        if (userTimeMoment <= timeToMoment && userTimeMoment >= timeFromMoment) {
                            newProposedIntervals.push(proposedIntervals[i])
                        }
                    }
                } else {
                    if (proposedIntervals.length !== 1) {
                        $reactions.answer('Могу предложить вам следующие интервалы на этот день:');
                    }
                    return proposedIntervals;
                }
            } else {
                $reactions.answer('К сожалению, у меня нет информации по интервалам доставки на этот день. Пожалуйста, выберите другой вариант:');
                return [];
            }
        }
        if (newProposedIntervals.length > 0) {
            if (newProposedIntervals.length !== 1) {
                $reactions.answer('Вот что я нашла:');
            }
            return newProposedIntervals;
        } else {
            if (proposedIntervals.length !== 1) {
                $reactions.answer('К сожалению, данное время не входит ни в один из возможных интервалов доставки. Могу предложить вам следующие интервалы на этот день:');
            }
            return proposedIntervals;
        }
    }

    function formatIntervalTitle(item, date) {
        date = (moment(date)).locale('ru').format('D MMMM');
        return item.replace(' ', ', ').replace(' с ', ', с ').replace(/(\d+)\.(\d+)\.(\d+)/, date);
    }

    function formatEditableUntil(date) {
        return (moment(date)).locale('ru').format('D MMMM, HH:mm');
    }

    return {
        loadDeliveryPrice: loadDeliveryPrice,
        checkOrderPrice:checkOrderPrice,
        formatDeliveryPrice:formatDeliveryPrice,
        fillIntervalsDct:fillIntervalsDct,
        intervalTimeSearch:intervalTimeSearch,
        formatIntervalTitle:formatIntervalTitle,
        formatEditableUntil:formatEditableUntil
    }

})();


