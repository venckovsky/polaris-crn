var YandexGeocoderUtils = (function(){

    function getYandexGeocoderResponse (addressString) {
        var bbox = setSearchLimitation()
        var url = "https://geocode-maps.yandex.ru/1.x/?apikey=863c7481-0e5a-4869-a7c8-8b244728e49f&format=json" + bbox + "&geocode="
        return $http.get(url + encodeURIComponent(addressString), {
            timeout: 7000,
        }).then(parseHttpResponse).catch(httpError);
    }

    function setSearchLimitation() {
        var limitation = '36.6,54.6~38.6,56.6';
        var deliveryTypeId = $jsapi.context().session.deliveryTypeId;
        if (deliveryTypeId == 2) {
            return '&bbox=36.6,54.6~38.6,56.6&rspn=1';
        } else {
            return '';
        }
    }

    function getNewFormatAddress(geoObject) {
        return geoObject.name + ' (' + geoObject.description + ')';
    }

    function parseGeoObject(geoObject) {
        var components;
        var pos;
        var parsedGeoObject = {};

        try {
            parsedGeoObject.formattedAddress = getNewFormatAddress(geoObject);

            components = geoObject.metaDataProperty.GeocoderMetaData.Address.Components;            
            for (var i = 0; i < components.length; i++) {
                if (components[i].kind === 'street') {
                    parsedGeoObject.street = components[i].name;
                } else if (components[i].kind === 'house') {
                    parsedGeoObject.house = components[i].name;
                }   
            }
        } catch (e) {
            log(e);
        }

        try {
            pos = geoObject.Point.pos;
            var splittedPos = pos.split(' ');
            if (splittedPos.length == 2) {
                parsedGeoObject.lat = parseFloat(splittedPos[1]);
                parsedGeoObject.lon = parseFloat(splittedPos[0]);
            }    
        } catch (e) {
            log(e);
        }
     
        if (parsedGeoObject.street && parsedGeoObject.house && parsedGeoObject.lat && parsedGeoObject.lon) {
            return parsedGeoObject;
        } else {
            return {};
        }
    }


    function getaddressesDct(geoObjectCollection) {
        var addressesDct = {};
        var newFormatAddress;
        geoObjectCollection.forEach(function(item) {
            if (item.GeoObject && item.GeoObject.name !== 'Москва'){
                newFormatAddress = getNewFormatAddress(item.GeoObject);
                addressesDct[newFormatAddress] = item.GeoObject;
            }    
        });
        return addressesDct;
    }

    function getAddressObject(address) {        
        getYandexGeocoderResponse(address).then(function(res) {
            var geoObjectCollection = res.response.GeoObjectCollection.featureMember;        
            $.session.addressesDct = {};         
            $.session.fullAddress = {};
            if (geoObjectCollection.length > 1) {
                $.session.addressesDct = getaddressesDct(geoObjectCollection);
            } else if (geoObjectCollection.length === 1){
                var geoObject = geoObjectCollection[0].GeoObject;
                if (geoObject && geoObject.name !== 'Москва'){
                    $.session.fullAddress = parseGeoObject(geoObject);
                } 
            } else {
                return;
            }
        }).catch(function (err) {
            log(err);
            return;
        });
    }



    return {
        getAddressObject: getAddressObject,
        parseGeoObject: parseGeoObject
    }

})();
