function PaginatorSession(session, temp, settings) {
    classCallCheck(this, PaginatorSession);

    var self = this;

    self.display = function (params) {
        self.log('display', params);

        if (params.iterator.isEmpty()) {
            self.log('iterator is empty. there is no pages to display!');
            return;
        }
        
        session.exitState = params.exitState;
        session.noMoreState = params.noMoreState || settings.paginatorState + '/NoMore';

        if (params.moreButtonText) {
            session.moreButtonText = params.moreButtonText;
        }

        setIterator(params.iterator);
        setPageProcessor(params.processor);

        self.currentPage();
    };

    self.nextPage = function () {
        self.log('move to next page');

        var iterator = self.getIterator();

        if (!iterator.hasNext()) {
            self.log('no more pages');
            settings.transition(session.noMoreState);
        } else {
            iterator.moveNext();
            self.currentPage();
        }
    };

    self.currentPage = function () {
        self.log('display current page');

        settings.transition(settings.paginatorState);

        var iterator = self.getIterator();
        var pageProcessor = self.getPageProcessor();

        pageProcessor.process(iterator.current());

        if (iterator.hasNext()) {
            addMoreButton();
        }

        setIterator(iterator);
        setPageProcessor(pageProcessor);
    };

    self.previousPage = function() {
        self.log('move to previous page');

        var iterator = self.getIterator();

        if (!iterator.hasPrevious()) {
            self.log('no more pages');
        } else {
            iterator.movePrevious();
        }
        self.currentPage();
    };

    self.exit = function (text) {
        var match = $nlp.match(text, session.exitState);
        self.log('exit to', match.targetState);

        $jsapi.context().parseTree = match.parseTree;

        settings.transition(match.targetState);
    };

    self.save = function (stateName) {
        session.stack = session.stack || [];


        if (stateName && findStateInStack(stateName) ) {
            throw new Error('Paginator already contains state with name ' + stateName);
        }

        session.stack.push({
            data: _.omit(session, 'stack'),
            name: stateName
        });
    };

    self.restore = function (stateName) {
        if (!self.canRestore()) {
            throw new Error('There is no saved states. Could not restore');
        }

        var data;

        if (stateName) {
            var state = findStateInStack(stateName);
            if (!state) {
                throw new Error('Paginator state ' + stateName + 'not found');
            }

            var stateIndex = session.stack.indexOf(state);

            session.stack = _.first(session.stack, stateIndex);

            data = state.data;
        } else {
            data = session.stack.pop().data;
        }

        self.reset(); //Invalidate current state including caches in temp
        _.extend(session, data); //Restore session to state from stack

        settings.transition(settings.paginatorState);

        self.currentPage();
    };

    self.canRestore = function() {
        return session.stack && session.stack.length > 0;
    };

    self.containsState = function (stateName) {
        return !!findStateInStack(stateName);
    };

    self.clear = function () {
        session.stack = [];
    };

    self.reset = function () {
        delete session.exitState;
        delete session.noMoreState;
        delete session.moreButtonText;
        delete session.moreButtonText;
        delete session.iterator;
        delete session.pageProcessor;
        delete temp.iterator;
        delete temp.pageProcessor;
    };

    function setIterator(iterator) {
        temp.iterator = iterator;
        session.iterator = Memento.makeMemento(iterator);
    }

    function setPageProcessor(processor) {
        temp.pageProcessor = processor;
        session.pageProcessor = Memento.makeMemento(processor);
    }

    self.getPageProcessor = function () {
        if (temp.pageProcessor) {
            return temp.pageProcessor;
        }
        else if (session.pageProcessor) {
            temp.pageProcessor = Memento.restore(session.pageProcessor);
            return temp.pageProcessor;
        }
    };

    self.getIterator = function () {
        if (temp.iterator) {
            return temp.iterator;
        }
        else if (session.iterator) {
            temp.iterator = Memento.restore(session.iterator); 
            return temp.iterator;
        }
    };

    function findStateInStack(stateName) {
        return session.stack && _.find(session.stack, function (v) {
            return stateName === v.name;
        });
    }

    function addMoreButton() {
        settings.moreButtonVisualiser(getMoreButtonText());
    }

    function getMoreButtonText() {
        return session.moreButtonText || settings.moreButtonText;
    }

    self.log = createServiceLogger('PAGINATOR', settings.logging);
}

function paginator() {
    return getOrCreateService('paginator', function (session, temp) {
        return new PaginatorSession(session, temp, {
            logging: {
                enabled: false,
                pretty: false
            },
            moreButtonText: 'Ещё',
            paginatorState: '/Paginator',
            moreButtonVisualiser: function (text) {
                if (isTelegram()){
                    $reactions.buttons({text: text, hide: true});
                }
                if (isYandex())
                    if ($.response.card) {
                        $.response.card.footer = {text: 'Ещё', button: {payload: {text: 'Ещё' , transition: '/Paginator/More'}}};
                    } else {
                        $reactions.buttons([{text: 'Ещё', transition: '/Paginator/More'}]);
                    }

            },
            transition: $reactions.transition
        });
    });
}