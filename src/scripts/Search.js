function SearchService(utkoClient, settings, temp) {
    classCallCheck(this, SearchService);

    var self = this;

    self.goods = function (text, opts) {

        self.log('searching for goods', text);

        var result = utkoClient.call('?method=CatalogSearch&QUERY=' + encodeURIComponent(text));
        var data = result.getApiData();
        var GoodsItemList = httpQueryResultToList(data);
        var sorted = sorting(GoodsItemList, 'PRODUCT_QUANTITY');
        result.GoodsItemList = httpQueryResultToList(data);

        $jsapi.context().session.cachedItemList = result.GoodsItemList; 

        return result;
    }

    self.log = createServiceLogger("SEARCH SERVICE", settings.logging);
}

function getIdByName (name, processor) {
        return _.findWhere(processor, {NAME: name});
    }


function search() {
    return getOrCreateService('search', function (temp) {
        return new SearchService(
            utkoClient(),
            {
                logging: {
                    enable: false,
                    pretty: true
                }
            },
            temp
        )
    });
}
