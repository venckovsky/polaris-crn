function classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
        throw new TypeError("Cannot call a class as a function");
    }
}

function getOrCreateService(name, sessionCreator) {
    var $temp = $.temp;
    var $session = $.session;

    $temp.services = $temp.services || {};
    var $services = $temp.services;

    if (!$services[name]) {
        $session[name] = $session[name] || {};
        $temp[name]    = $temp[name] || {};

        $services[name] = sessionCreator($session[name], $temp[name]);
    }
    return $services[name];
}

function makeLongObjectsReplacer(maxSize, replacer, exampleSize) {
    return function (key, value) {
        var v = value;

        var size;

        if (_.isArray(value)) {
            size = _.size(value);

            if (size > maxSize) {
                var striped = value.slice(0, exampleSize);

                if (!striped) {
                    //Hack to workaround strange nashorn bug when Array.prototype.slice sometimes returns undefined
                    striped = JSON.parse(JSON.stringify(value)).slice(0, exampleSize);
                }

                v = striped.concat("..." + (size - exampleSize) + " more elements")
            }

        } else if (_.isObject(value)) {
            size = _.size(value);

            if (size > maxSize) {
                var dataKeys = _.keys(value).filter(function (k) {
                    return !_.isFunction(value[k]);
                });

                size = dataKeys.length;

                v = _.pick(value, _.first(dataKeys, exampleSize));
                v["..."] = (size - exampleSize) + " more elements";
            }
        }

        if (replacer && _.isFunction(replacer)) {
            return replacer(key, v);
        } else {
            return v;
        }
    }
}

function createServiceLogger(name, settings) {
    return function (label, obj, replacer, space) {
        if (settings.enabled) {

            var r = settings.maxSize
                ? makeLongObjectsReplacer(settings.maxSize, replacer, settings.exampleSize || 5)
                : replacer;

            var s = settings.pretty
                ? (_.isUndefined(space) ? 2 : space)
                : undefined;

            var logStr = "[" + name + "] " + label;

            if (!_.isUndefined(obj)) {
                logStr += (":\n" + JSON.stringify(obj, r, s));
            }

            log(logStr);
        }
    }
}

function httpQueryResultToList(data) {
    var key;
    var CategoryList = new Array();
    for (key in data) {
        if (key != 'status') {
            CategoryList.push(data[key]);
        };
    };
    return CategoryList;
}

function sorting(array, parameter) {
    var sorted = array.sort(function(a, b) {
        return (a.parameter<b.parameter) - (b.parameter<a.parameter);
    });
}