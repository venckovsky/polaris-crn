function getAuthCodes() {
    return $http.get('http://www.utkonos.ru/jrpc/test/sms/auth/code', {
        headers:{ Authorization: "Basic dXRrb3Byb2Q6UHJvZFVUS09tQXQyMjc5"}, 
        timeout: 7000
    }).then(parseHttpResponse).catch(httpError);
}

function getCodeForPhoneNumber(phoneNumber) {
    var $session = $jsapi.context().session;
    var code;
    getAuthCodes().then(function (res) {
        var codes = res.result.codes;
        if (codes && codes.length > 0) {
            for(var i = 0; i < codes.length; i++) {
                if (codes[i].phone === phoneNumber) {
                    code = codes[i].code.trim();
                    $jsapi.context().parseTree = {"tag": "root","pattern": "root","text": code,"words": [code],"code": [{"tag": "code","pattern": "code","text": code,"words": [code]}],"_code": code,"_Root": code} 
                    break;
                }
            }   
        }
    }).catch(function (error) {
        $reactions.answer(error); 
    });
}
