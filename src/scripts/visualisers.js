function isTelegram() {
    return $.request.channelType === 'telegram';
}

function isYandex() {
    return $.request.channelType === 'yandex';
}

function toFixed(numberString){
    var fixedNumberString = parseFloat(numberString).toFixed(2).toString();

    return fixedNumberString;
}

function declination(numberString) {
    var sum = numberString.toString().split('.');
    if (sum[1] !== undefined) {
        var kopecks = sum[1] * 10;
        return sum[0] + ' ' + $nlp.conform('рубль', sum[0]) + ' ' + kopecks + ' ' + $nlp.conform('копейка', kopecks);
    } else {
        return sum[0] + ' ' + $nlp.conform('рубль', sum[0]);
    }
}

function toReadable(UnitList) {
    if (UnitList == undefined) {
        return '';
    } else if (UnitList[0].BasicUnit == "KG") {
        return "за кг";
    } else {
        return '';
    }
}

function visualiseGoodsPage(page) {
    $reactions.answer('Вот что я нашла:', 'Cмотрите, что можно купить:', 'Вот какие есть варианты:');

    if (isTelegram()) {
        page.forEach(function (item) {
            var inStock = item.PRODUCT_QUANTITY != 0 && item.PRODUCT_QUANTITY != null ? 'В наличии ' + item.PRODUCT_QUANTITY + ' шт.' : 'Нет в наличии';
            var price = item.PRICE != null ? 'Цена: ' + item.PRICE : '';
            $reactions.answer('<a href="' + item.PICTURES[0] + '">' + item.NAME + '</a>' + '\n' + price + '\n' + inStock);
            _.last($.response.replies).markup = 'html';
        });

        page.forEach(function (item) {
            $reactions.buttons({text: item.NAME});
        });
    }

    else if (isYandex()) {
        var items = [];

        page.forEach(function (item) {
            items.push({
                image_id: getYandexUrlId(item.ImageHighUrl, yaUtkoUrls),
                title: item.NAME,
                description: toFixed(item.PRICE) + ' руб.',
                button: {payload: {text: item.NAME, transition: '/Catalog/Search'}}
            });
        });

        $.response.card = {type: 'ItemsList', items: items};

        /*if (cart().isEmpty() === false) {
            $reactions.buttons([{text: 'Оформить заказ', hide: true, transition: '/Cart/Show'}]);            
        } else {
            $reactions.buttons([{text: 'Помощь', hide: true, transition: '/Help'}]);
        }*/
        $reactions.buttons([{text: 'Оформить заказ', hide: true, transition: '/Cart/Show'}, {text: 'Помощь', hide: true, transition: '/Help'}]);
        
    }     
}

function visualiseGoodsDetails(item, showBackButton) {
    var $session = $jsapi.context().session;
    var currentItem = catalog().getItem($session.currentItem.ID);
    if (isTelegram()) {
        $reactions.answer('Вот информация об этом товаре.');
        $reactions.answer(
            '<a href="' + currentItem.PICTURES[0] + '">' + currentItem.NAME + '</a>' + '\n'
            + 'Изображение: ' + currentItem.PICTURES[0] + '\n'
            + (currentItem.PRICE != null ? 'Цена: ' + currentItem.PRICE  + '\n' : '')
            + (currentItem.PRODUCT_QUANTITY != null ? 'Количество на складе: ' + currentItem.PRODUCT_QUANTITY + '\n' : 'Нет в наличии.')
        );

        _.last($.response.replies).markup = 'html';
    }
    
    if (isYandex() || testMode()) {
        var image_id = getYandexUrlId(currentItem.ImageHighUrl, yaUtkoUrls);
        var title = currentItem.NAME; 
        var description = 'Цена: ' + toFixed(currentItem.PRICE) + ' руб. ' + toReadable(currentItem.GoodsUnitList)
            + '<a href="' + currentItem.PICTURES[0] + '">' + currentItem.NAME + '</a>' + '\n'
            + 'Изображение: ' + currentItem.PICTURES[0] + '\n'
            + (currentItem.PRICE != null ? 'Цена: ' + currentItem.PRICE  + '\n' : '')
            + (currentItem.PRODUCT_QUANTITY != null ? 'Количество на складе: ' + currentItem.PRODUCT_QUANTITY + '\n' : 'Нет в наличии.');
                   
        $reactions.answer('Вот информация об этом товаре.');           
        $.response.card = {type: 'BigImage', image_id: image_id, title: title, description: description};
    }
    var favouritesList = favourites().getFavouritesList();
    var isFavourite = _.findWhere(favouritesList, {PRODUCT_ID: currentItem.ID});
    var cartList = cart().lookup();
    var isInCart = _.findWhere(cartList, {PRODUCT_ID: currentItem.ID});
    if (isInCart == undefined) {
        $reactions.buttons([{text: 'Добавить в корзину', hide: true, transition: '/Catalog/AddToCart'}]);
    } else {
        $reactions.buttons([{text: 'Удалить из корзины', hide: true, transition: '/Cart/Modify/ModifyQuantity/IfDelete'}]);
    }
    if (isFavourite == undefined) {
        $reactions.buttons([{text: 'Добавить в избранное', hide: true, transition: '/Favourites/Add'}]);
    } else {
        $reactions.buttons([{text: 'Удалить из избранного', hide: true, transition: '/Favourites/Delete'}]);
    }
    if (showBackButton) {
        $reactions.buttons([{text: 'Назад', hide: true}]);
    } else {
        $reactions.buttons([{text: 'Продолжить покупки', hide: true, transition: '/Catalog/Main'}]);

    }
}


function visualiseCategoriesPage(page) {
    var $temp = $jsapi.context().temp;
    if (!$temp.fromMainCategories){
        $reactions.answer("Вот, что есть в этой категории:", "Выберите подкатегорию:", "Нажмите на кнопку с нужной категорией: ");
    }
    for (var i = 0; i < page.length; i += 2) {
        var b1 = page[i];
        var b2 = page[i + 1];

        var buttons = [{text: b1.NAME, transition: '/Catalog/Main/Category'}];
        if (b2) {
            buttons = buttons.concat([{text: b2.NAME, transition: '/Catalog/Main/Category'}]);
        }

        $reactions.buttons(buttons);
    }

/*    if (page[0].ParentId !== '0') {
        $reactions.buttons({text: 'назад'});
    }
*/
}

function getGoodsListText(goodsList) {
    var goodsListText = '';
    var price;

    for (var i = 1; i <= goodsList.length; i++) {
        price = goodsList[i-1].PriceTotal || goodsList[i-1].Price;
        goodsListText += i.toString() + '. ' + goodsList[i-1].Name + '\n Кол-во: '  + 
                        goodsList[i-1].Quantity + '\n Cтоимость: ' + toFixed(price) + ' руб.\n\n';
    }

    return goodsListText;
}


function visualiseCart(goodsList) {
    var goodsListText = '';
    var totalSum = 0;
    var checkOrderCount = checkOrder();

    for (var i = 1; i <= goodsList.length; i++) {
        goodsListText += i.toString() + '. ' + goodsList[i-1].NAME + '\n Кол-во: '  + 
                        goodsList[i-1].QUANTITY + '\n Cтоимость: ' + toFixed(goodsList[i-1].QUANTITY * goodsList[i-1].PRICE) + ' руб.\n\n';
        totalSum +=  goodsList[i-1].QUANTITY * goodsList[i-1].PRICE;                    
    }
    if (goodsList.length < 1) {
        $reactions.answer("Ваша корзина пуста.");
        $reactions.transition("/Catalog/Main");
    } else {
        $reactions.answer({value: 'Вы заказали: \n', tts: 'Вы заказали на сумму ' + totalSum + '.'});
        $reactions.answer({value: goodsListText, tts: ' '});
        $reactions.answer({value: 'Общая стоимость заказа: ' + toFixed(totalSum) + ' руб.\n', tts: ' '});
        $reactions.buttons([{text: 'Заказать ещё', hide: true, transition: '/Catalog/Main'},
                        {text: 'Изменить', hide: true, transition: '/Cart/Modify'}, 
                        {text: 'Выбрать адрес', hide: true, transition: '/Cart/ConfirmCart'}]);
    }
      
}
function visualisePreOrderCart(goodsList, totalSum) {
    var goodsListText = '';
    var totalSum = 0;
    var checkOrderCount = checkOrder();

    for (var i = 1; i <= goodsList.length; i++) {
        goodsListText += i.toString() + '. ' + goodsList[i-1].NAME + '\n Кол-во: '  + 
                        goodsList[i-1].QUANTITY + '\n Cтоимость: ' + toFixed(goodsList[i-1].QUANTITY * goodsList[i-1].PRICE) + ' руб.\n\n';
        totalSum +=  goodsList[i-1].QUANTITY * goodsList[i-1].PRICE;                    
    }
    if (goodsList.length < 1) {
        $reactions.answer("Ваша корзина пуста.");
        $reactions.transition("/Catalog/Main");
    } else {
        $reactions.answer({value: 'Вы заказали: \n', tts: 'Вы заказали на сумму ' + declination(totalSum) + '.'});
        $reactions.answer({value: goodsListText, tts: ' '});
        $reactions.answer({value: 'Общая стоимость текущей корзины: ' + toFixed(totalSum) + ' руб.\n', tts: ' '});
        $reactions.buttons([{text: 'Заказать ещё', hide: true, transition: '/Catalog/Main'},
                        {text: 'Изменить', hide: true, transition: '/Cart/Modify'}, 
                        {text: 'Добавить к заказу', hide: true, transition: '/Order/OrderModify/OrderAppend'}]);
    }
}


function visualiseCartModification(goodsList, getItemTransition) {
    var nameToItem = {};
    $reactions.answer('Нажмите на кнопку с товаром, который вы хотите изменить или удалить: ');
    goodsList.forEach(function (item) {
        nameToItem[item.NAME] = item;
        $reactions.buttons({text: item.NAME, transition: getItemTransition});

    });
    $reactions.buttons([{text: 'Удалить всё', hide: true, transition: '/Cart/ConfirmDeletion'}, 
                                {text: 'Закончить редактирование', hide: true, transition: '/Cart/Show'}]);
}

function visualiseCurrentItem(item, backTransition) {
    var buttons = [];
    
    var currentItemDescr = '\n' + item.NAME + ' - ' + toFixed(item.PRICE)  + ' руб.\nКол-во: ' + item.QUANTITY + '\n'; 
    $reactions.answer({value: currentItemDescr, tts: ' '});
    buttons.push({text: '+1'}, {text: '-1'});
    
    if (backTransition) {
        buttons.push({text: 'Назад', hide:true, transition: backTransition});

        if (!$.session.showModifyQuantityInstruction) {
            $.session.showModifyQuantityInstruction = true;
            $reactions.answer('Вы можете поменять количество товара с помощью кнопок «+1» и «-1».');
        }
    }
    
    $reactions.buttons(buttons);
}

function visualiseModifiedItem(item, backTransition) {
    $reactions.buttons([{text: 'Оформить заказ', hide: true, transition: '/Cart/Show'}, {text: 'Продолжить покупки', hide: true, transition: '/Catalog/Main'}], [{text: 'Назад', hide:true, transition: backTransition}]);
}

function visualiseItemForModification(item, backTransition) {
    var buttons = [];
    $reactions.answer('Вы можете поменять количество товара с помощью кнопок «+1» и «-1».');
    buttons.push({text: '+1'}, {text: '-1'});
    
    if (backTransition) {
        buttons.push({text: 'Назад', hide:true, transition: backTransition});
    }
    
    $reactions.buttons(buttons);
}


function getRandomGoodWord() {
    var suggestGoods = ['Чайник', 'Соковыжималка', 'Мультиварка', 'Обогреватель'];
    return shuffle(suggestGoods)[0];
}
/////////////////////////// DELIVERY ///////////////////////////


function visualiseTerminalsPage(page) {
    if (isTelegram()) {
        var answer;
        var buttons = [];
        var itemDescr;

        page.forEach(function (item) {
            if (item.Metro.length > 0) {
                itemDescr = '\nМетро:';
                item.Metro.forEach(function (metroStation) {
                    itemDescr += '\n •' + metroStation.Name;
                });
            } else {
                itemDescr = '';
            }

            answer = item.Title + itemDescr;
            buttons.push({text: item.Title, transition: '/Delivery/Start/Foodbox/GetFoodboxId'});
            $reactions.answer(answer);

        });

        $reactions.buttons(buttons);
    }

    else if (isYandex()) {
        var items = [];
        var itemDescr;

        page.forEach(function (item) {
            if (item.Metro.length > 0) {
                itemDescr = '\nМетро:';
                item.Metro.forEach(function (metroStation) {
                    itemDescr += '\n •' + metroStation.Name;
                });
            } else {
                itemDescr = '';
            }
            
            items.push({
                title: item.Title,
                button: {payload: {text: item.Title, transition: '/Delivery/Start/Foodbox/GetFoodboxId'}} 
            });     
            if (itemDescr) {
                items[items.length - 1].description = itemDescr;
            }
        });

        $.response.card = {type: 'ItemsList', items: items};

        /*if (cart().isEmpty() === false) {
            $reactions.buttons([{text: 'Оформить заказ', hide: true, transition: '/Cart/Show'}]);            
        } else {
            $reactions.buttons([{text: 'Помощь', hide: true, transition: '/Help'}]);
        }*/
        $reactions.buttons([{text: 'Оформить заказ', hide: true, transition: '/Cart/Show'}, {text: 'Помощь', hide: true, transition: '/Help'}]);
    }
}

function visualiseIntervalsPage(page) {
    if (isTelegram()) {
        var answer;
        var buttons = [];
        page.forEach(function (item) {
            //answer = item.Title;
            answer = DeliveryUtils.formatIntervalTitle(item.Title, item.Date);
            buttons.push({text: item.Title});
            if(item.Price === 0) {
                answer += '\nДоставка бесплатно';
            } else {
                answer += '\nДоставка — ' + item.Price;
            }
            $reactions.answer(answer);
        });

        $reactions.buttons(buttons);
    }

    if (isYandex()) {
        var itemEl;
        var items = [];
        page.forEach(function (item) {
            itemEl = {};
            //itemEl.title = item.Title;
            itemEl.title = DeliveryUtils.formatIntervalTitle(item.Title, item.Date);
            itemEl.description = '';
            if (item.Price === 0) {
                itemEl.description += 'Доставка бесплатно';
            } else {
                itemEl.description += 'Доставка — ' + item.Price;
            }
            itemEl.button = {payload: {text: item.Title}};
            items.push(itemEl);
        });
        $.response.card = {type: 'ItemsList', items: items};
        $reactions.buttons([{text: 'Изменить время', hide: true, transition: '/Delivery/Intervals'}]);
    }
}
    

function visualiseUserAddressList(addressInfo, transition) {
    var buttons = [];

    addressInfo.forEach(function (item) {
        buttons.push({text: item, transition: transition});
    });
    $reactions.buttons(buttons);
}



function visualiseAddressList(addressesDct) {
    var buttons = [];
    for (var i = 0; i < 5 && i < Object.keys(addressesDct).length; i++) {
        var addressPrecision = addressesDct[Object.keys(addressesDct)[i]]['metaDataProperty']['GeocoderMetaData']['precision'];
        if (addressPrecision == 'number' || addressPrecision == 'exact') {
            buttons.push({text: Object.keys(addressesDct)[i], transition: './ChosenAddress'});
        }
    }
    if (buttons.length > 0) {
        buttons.push({text: 'Моего адреса нет в списке', transition: '/Delivery/Start/Address/AddressByPiece'});
        $reactions.answer('Выберите правильный адрес:');
        $reactions.buttons(buttons);
    } else {
        $reactions.transition('/Delivery/Start/Address/AddressByPiece');
    }
}

function getRandomGoodWord() {
    var suggestGoods = ['Чайник', 'Соковыжималка', 'Мультиварка', 'Обогреватель'];
    return shuffle(suggestGoods)[0];
}


/////////////////////////// ORDER ///////////////////////////



function getOrderItemInfo(item) {
    var buttonTitle;
    var title;
    var description;
    var status = item.CANCELED == 'Y' ? 'Отменён' : 'Оформлен';
    
    buttonTitle = item.ID;
    title = 'Заказ ' + buttonTitle + ' (' + status + ')';
    description = toFixed(item.PRICE) + ' руб.';
    

    return {
        buttonTitle: buttonTitle,
        title: title, 
        description: description};

}


function visualiseOrdersPage(page) {
    var itemInfo;
    var items = [];
    var buttons = [];
    var transition = '/Order/OrderHistory/GetOrderId';

    if (isTelegram()) {
        page.forEach(function (item) {
            itemInfo = getOrderItemInfo(item);
            $reactions.answer(itemInfo.title + '\n' + itemInfo.description);
            buttons.push({text: itemInfo.buttonTitle, transition: transition});
        });
        $reactions.buttons(buttons);
    }

    else if (isYandex()) {
        page.forEach(function (item) {
            itemInfo = getOrderItemInfo(item);
            items.push({
                title: itemInfo.title,
                description: itemInfo.description,
                button: {payload: {text: itemInfo.buttonTitle, transition: transition}} 
            });
        });

        $.response.card = {type: 'ItemsList', items: items};
    }
}


function visualiseOrderLookup(order){
    var answer = '';
    var status = order.CANCELED == 'Y' ? 'Отменён' : 'Оформлен';


    answer += 'Заказ ' + order.ID;
    answer += '\n' + status;
//    answer += '\n\nДоставка: ' + orderInfo.DeliveryAddress + ', ' + orderInfo.DeliveryTimeTitle;
//    answer += '\nОплата наличными при получении.'; можно кешировать ответ метода GetDelivery в самом начале
    answer += '\nСтоимость товаров: ' + toFixed(order.PRICE) + ' руб.';
    if (order.PRICE_DELIVERY !== '0') {
        answer += '\nСтоимость доставки: ' + toFixed(order.PRICE_DELIVERY) + ' руб.';
        answer += '\nИтого: ' + toFixed(order.PRICE) + ' руб.';
    }
/*    if (orderInfo.OrderItemList){
        goodsList = getGoodsListText(orderInfo.OrderItemList);
        answer += '\n\nТовары в заказе:';
        answer += '\n' + goodsList;
    }
*/
    $reactions.answer({value: answer, tts: 'Вот информация по вашему заказу.'});
    $reactions.buttons([{text: 'Назад', hide: true, transition: '/Order/OrderHistory'}]);
    if (order.CANCELED == 'N') {
        $reactions.buttons([{text: 'Отменить заказ', hide: true, transition: '/Order/OrderCancel'}]);
    }
}

function visualiseTemplatesPage(page) {
    $reactions.answer("Список шаблонов: ");

    for (var i = 0; i < page.length; i += 2) {
        var b1 = page[i];
        var b2 = page[i + 1];

        var buttons = [{text: b1.ORDER_TEMPLATE_NAME, transition: '/Templates/Show/GetTemplateId'}];
        if (b2) {
            buttons = buttons.concat([{text: b2.ORDER_TEMPLATE_NAME, transition: '/Templates/Show/GetTemplateId'}]);
        }

        $reactions.buttons(buttons);
    }
}

function visualiseTemplate(template) {
    var answer = '';

    answer += 'Шаблон ' + template.ORDER_TEMPLATE_NAME + '.';
    answer += '\nна основе заказа ' + template.ORDER_TEMPLATE_ID + '.';

    $reactions.answer(answer);
    $reactions.buttons([{text: 'Оформить быстрый заказ по шаблону', hide: true, transition: '/Templates/CreateOrderWithTemplate'},
                        {text: 'Назад', hide: true, transition: '/Templates/Show'},
                        {text: 'Удалить шаблон', hide: true, transition: '/Templates/Remove'}]);
}

function visualiseFavouritesPage(page) {
    var $session = $jsapi.context().session;
    var checkOrderCount = checkOrder();
    $session.favList = [];
    $reactions.answer({value: 'В Избранном: \n', tts: ' '});
    page.forEach(function (item) {
        var favouritesItem = catalog().getItem(item.PRODUCT_ID);
        $reactions.buttons({text: favouritesItem.NAME});
        $session.favList.push(favouritesItem.NAME);
    });
    $reactions.buttons([
                {text: 'Продолжить покупки', hide: true, transition: '/Catalog/Main'}]);
}  
