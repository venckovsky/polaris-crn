function exitFromSkill() {
    $jsapi.context().response.end_session = true;
    $reactions.transition("/");
}

function shuffle(array) {
    if (testMode()) {
        return array;
    } else {
        return _.shuffle(array);
    }
}

function dateIsPast(date) {
    return moment().isAfter(moment(date).subtract({hours: 3}));
}

function getYandexUrlId(origUrl, yamlUrlList){
    var yaUrl;
    
    if (origUrl) {
        origUrl = origUrl.replace(/\?.*/, '');
    } 
    
    if (origUrl in yamlUrlList){
        yaUrl = yamlUrlList[origUrl];
    } else {
        yaUrl = yamlUrlList['default'];
    }

    var id = yaUrl.replace('https://avatars.mds.yandex.net/get-dialogs-skill-card/', '').replace('/orig', '');
    return id;  
} 

function isEmptyObj(obj) {
    return Object.keys(obj).length === 0;
}

function getGeneralRareAnswer(commonFeild, counterField, contentField, indexField, top, bottom){
    function removeElementFromArray(arr, value){
        var index = arr.indexOf(value);
        if (index >= 0) {
            arr.splice(index, 1 );
        }
        return arr
    }

    var $client = $jsapi.context().client;
    var counter = $client[commonFeild][counterField];
    var content = $client[commonFeild][contentField];
    var currentAnswerIndex = $client[commonFeild][indexField];
    if (counter == currentAnswerIndex){
        if (content.length == 0 || content == undefined){
            content = refillContent(commonFeild, contentField);
        }
        var conti = randomInteger(0, content.length-1);
        var answer = content[conti];
        removeElementFromArray(content, answer);
        $client[commonFeild][contentField] = content;
        $client[commonFeild][indexField] = randomInteger(top, bottom); 
        $client[commonFeild][counterField] = 1;
        return answer
    }
    $client[commonFeild][counterField] += 1;
    return false
}