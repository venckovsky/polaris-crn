require: patterns.sc
  module = common

theme: /Favourites

    state: Show
        q!: * ((покажи|показать|перейти|перейди|открыть|открой|*смотреть) * ~избранное) *
        q!: ~избранное
        q!: * {(что|товар*|найти) * ~избранное} * 
        script:
            var favouritesList = favourites().getFavouritesList();
            var iterator = new ArrayPageIterator(favouritesList);
                paginator().display({
                    exitState: '/Catalog/Main',
                    noMoreState: '/Catalog/Main/',
                    iterator: iterator,
                    processor: new FavouritesPageProcessor()
                });

    state: Add
        q!: * ((добав*|положи*) * ~избранное) *
        q!: * в избранное *
        script:
            $temp.favouritesAdd = favourites().favAdd($session.currentItem.ID);
            $reactions.answer('Товар добавлен в Избранное.');
            $reactions.buttons([{text: 'Избранное', hide: true, transition: '/Favourites/Show'}, {text: 'Корзина', hide: true, transition: '/Cart/Show'}, {text: 'Продолжить покупки', hide: true, transition: '/Catalog/Main'}])

    state: Delete
        q!: * ((удал*|убери*|убрать) * ~избранное) *
        q!: * из ~избранное *
        script:
            $temp.favouritesDel = favourites().favDel($session.currentItem.ID);
            $reactions.answer('Товар удален из Избранного.');
            $reactions.buttons([{text: 'Избранное', hide: true, transition: '/Favourites/Show'}, {text: 'Корзина', hide: true, transition: '/Cart/Show'}, {text: 'Продолжить покупки', hide: true, transition: '/Catalog/Main'}])