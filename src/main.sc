require: common.js
  module = common

require: patterns.sc
  module = common

require: dateTime/moment.min.js
  module = common

require: number/number.sc
  module = common

require: dateTime/dateTime.sc
  module = common

require: dateTime/dateTimeConverter.js
  module = common

require: city/city.sc
  module = common


require: patterns.sc
require: paginator.sc
require: catalog.sc
require: cart.sc
require: auth.sc
require: delivery.sc
require: order.sc
require: integrationTest.sc
require: easterEggs.sc
require: favourites.sc
require: order_templates.sc

require: scripts/answer.js
require: scripts/utility.js
require: scripts/serviceUtils.js
require: scripts/memento.js
require: scripts/iterators.js
require: scripts/visualisers.js
require: scripts/Paginator.js
require: scripts/errors/utkonosErrors.js
require: scripts/UtkoClient.js
require: scripts/User.js
require: scripts/Search.js
require: scripts/Catalog.js
require: scripts/Cart.js
require: scripts/Delivery.js
require: scripts/yandexGeocoderUtils.js
require: scripts/deliveryUtils.js
require: scripts/Order.js
require: scripts/servicesFunctions.js
require: scripts/integrationTest.js
require: scripts/deliveryIntervals.js
require: scripts/favourites.js
require: scripts/Templates.js


init:
    $global.newSession = function($context) {
        $context.request.data.newSession = true;
        $context.request.data.client = $context.client;
        $reactions.newSession({message: $context.request.query, data: $context.request.data});
    }

    $global.$ = {
        __noSuchProperty__ : function (property) {
            return $jsapi.context()[property];
        }
    };

    bind("postProcess", function($context) {
        //Нужен для CatchAll
        $context.session.lastState = $context.contextPath;

        $context.session.lastActiveTime = $jsapi.currentTime();
        if ($context.request && $context.request.rawRequest && $context.request.rawRequest.session && $context.request.rawRequest.session.session_id) {
            $context.client.yaSessionId = $context.request.rawRequest.session.session_id;
        }
    });

    bind("preProcess", function($context) {
        if ($context.request && $context.request.rawRequest && $context.request.rawRequest.session) {
            var yaSession = $context.request.rawRequest.session;
        }
        if ($context.request.query && $context.session.lastActiveTime) {
          var interval = $jsapi.currentTime() - $context.session.lastActiveTime.valueOf();

          if (interval > 120 * 60 * 1000) {
            newSession($context);
          } else {
            if (yaSession && yaSession.session_id) {
                var yaSessionId = $context.request.rawRequest.session.session_id;
            }
            if ($context.client.yaSessionId && yaSessionId && $context.client.yaSessionId != yaSessionId) {
              newSession($context);
            }
          }
        }
        if ($context.request && $context.request.data && $context.request.data.client) {
            $context.client = $context.request.data.client;
        }
        if (yaSession && yaSession.new == true) {
          switch(yaSession.skill_id) {
            case "justaiUtkonos":
              $context.temp.targetState = "/Activation";
              break;                         
          }
        }
        var previousState = $context.contextPath || '/';
        var curState = $context.currentState || '/';
        log('|TransitionLog|' + $context.request.query + '|' + previousState + '|' + curState + '|');
    });

    $jsapi.bind({
        type: "preMatch",
        handler: function($context)
        {
            if (!$context.request.query) {
                $context.temp.targetState = "/Activation";
            }
        },
        path: "/",
        name: "avoidNullUtterance"
    });

patterns:
    $hurryUpCommands = (* (быстре*/скоре*/*торопись/тормоз*/ускор*/*шустре*) *)

theme: /

    state: Activation
        q!: test
        q: * поларис* *
        script:
            var randomGood = getRandomGoodWord();
            //if (cart().isEmpty() === false) {
            //    $reactions.answer('Вы можете продолжить оформление заказа.');
            //} else {
                $reactions.answer('Я помогу оформить заказ в интернет-магазине бытовой техники «Polaris».');
                $reactions.answer('Назовите товар, который бы вы хотели приобрести, например, «' + randomGood + '», или воспользуйтесь кнопками-подсказками.');
                //для Беты
            //}
            $reactions.buttons([{text: 'Мои заказы', hide: true, transition: '/Order/OrderHistory'}]);
        go!: /Catalog/Main

    
    state: Exit 
        q: $stopGame
        a: Если захотите заказать что-нибудь ещё, скажите «Вызови Утконос»!
        script: 
            exitFromSkill();

    state: reset
        q!: * (*reset|ресет)
        q!: * *start
        script:
          $context.session = {}
          $context.client = {}
          $context.temp = {}
          $response = {}
        go: /

    state: CatchAll || noContext = true
        q!: *
        a: Попробуйте переформулировать вопрос или следуйте кнопкам меню.
        if: $session.lastState
            go!: {{ $session.lastState }}  

    state: Obscene || noContext = true
        q!: * $mat *
        q!: * {$mat * $strictYou} *
        script:
            if ($parseTree.strictYou) {
                $reactions.answer("Не очень-то приятно это слышать.", "Эх. Нормально же общались.", "Обидеть бота может каждый.");
            } else {
                $reactions.answer("Очень оригинально. Сарказм.", "Давайте договоримся, Вы не используете такую лексику, а я ... Ещё не придумала. Но тоже как-нибудь себя ограничу.");
            }

    state: GoTo || noContext = true
        q!: justai goto $Text
        a: Выполняется переход в состояние /{{$parseTree.Text[0].value}}.
        go!: /{{$parseTree.Text[0].value}}

    state: TestModeOnOff || noContext = true
        q!: justai test (on:1|off:2)
        if: ($parseTree._Root == "1")
            script:
                $session.justaiTest = true;
            a: Тестовый режим включён.
        else:
            script:
                delete $session.justaiTest;
            a: Тестовый режим выключен.

    state: setTimeout || noContext = true
        q!: justai setTimeout $Text $Number
        script:
            utkoClient().setTimeoutForApiMethod($parseTree._Text, $parseTree._Number);
            $reactions.answer(JSON.stringify(utkoClient().getAllTimeouts(), null, 4));


    state: clearTimeouts || noContext = true
        q!: justai clearTimeouts
        script:
            utkoClient().clearTimeoutsForApiMethods();
            $reactions.answer(JSON.stringify(utkoClient().getAllTimeouts(), null, 4));

    state: ping || noContext = true
        q!: ping
        a: Я работаю в штатном режиме.