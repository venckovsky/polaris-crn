theme: /integrationTestSMSCode

    state: Test
        q!: integration test sms code
        script:
            getCodeForPhoneNumber($client.UtkonosPhoneNumber);
        go!: /Authorisation/AskCode/Code

    state: TestEmptyCart
        q!: test empty cart
        script:
            cart().clear();