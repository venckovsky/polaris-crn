require: patterns.sc
  module = common

require: cart.sc

require: scripts/visualisers.js
  

patterns: 
    $get = (*купи*/найди/~заказать/~получить/~приобрести/на (ужин/обед/завтрак)/~поесть/~съесть/~добавить/~прибавить/ещё/есть/[ещё] [мне] (нужны/нужно/нужна/надо))
    $wantPhrases = ((хочу/желаю/давай/дай) [$get]/$get)
    $search = (~искать/~находить/~найти/[в/из] (каталог*/поиск*))
    $units = (упаков*/пачк*/шт/штук*/короб*/коробоч*/стаканчик*/бутыл*)
    $toWhom = (мне/нам/для (меня/нас))

theme: /Catalog

    state: Main
        q!: * [открой/покажи/расскажи] * каталог* * [товаров] *
        q!: * что * (можно/я могу) купить *
        q!: * (какие/расскажи/скажи) * (товары/товарах) * 
        q!: * что * есть *
        q!: * хочу {что-нибудь купить} *
        q!: заказать ещё
        q!: [новый] заказ
        q: * (добавить/добавь) * || fromState = /Order/OrderModify, onlyThisState = true
        script:
            var categories = catalog().getCategories();
            var iterator = new ArrayPageIterator(categories);
                paginator().display({
                    exitState: '/Catalog/Main',
                    noMoreState: '/Catalog/Main/',
                    iterator: iterator,
                    processor: new CategoriesPageProcessor()
                });

        state: NoMore 
            script:
                $temp.noQuestionAboutProduct = true;
            a: Других категорий, к сожалению, пока нет в нашем магазине. Посмотрите повнимательнее! Может, вам всё же понравится что-то из нашего каталога.
            go!: /Catalog/Main 

    
        state: Category
                q: {[* $wantPhrases] $Text} [ещё] || fromState = /Catalog/Main, onlyThisState = true
                script:
                    var category;
                    var match;
                    var query = ($parseTree && $parseTree._Text) || $.request.query;
                    var pageProcessor = paginator().getPageProcessor();

                    if (pageProcessor) {
                        match = $nlp.matchExamples(query, Object.keys(pageProcessor.items));
                        if (match) {
                            category = pageProcessor.items[match.example];
                        }
                    }

                    if (category) {
                        $session.textToSearch = category.NAME;
                        $reactions.transition('/Catalog/Search/DoSearch');
                    } else {
                        $session.textToSearch = query;
                        $reactions.transition('/Catalog/Search/DoSearch');
                    }     

        state: SearchInCatalog
            q: * $search $Text
            q: $Text $search *
            script:
                $session.textToSearch = $parseTree._Text;
                $reactions.transition('/Catalog/Search/DoSearch');                

    state: Search
        q!: {[$wantPhrases][$toWhom][$Number][$units] $Text [ещ*]}
        script:
            var previousQueryResult = $session.cachedItemList;
            $session.textToSearch = $parseTree._Text || $session.textToSearch;
            if (previousQueryResult) {
                log('prettyPrevious: ' + toPrettyString(previousQueryResult));
                if (isYandex() && $request.rawRequest && $request.rawRequest.request && $request.rawRequest.request.command) {
                    var res = getIdByName($request.rawRequest.request.command, previousQueryResult);
                } else {
                    var res = getIdByName($parseTree.text, previousQueryResult);
                    log('parsetreeTextBefore: ' + $parseTree.text)

                }

                if (res) {
                log('fromPreviousQuery: ' + res + toPrettyString(res) + 'parsetreeTextAfter: ' + $parseTree.text);
                    $session.currentItem = res;
                    $reactions.transition('./VisualiseItem');
                } else {
                    $reactions.transition('/Catalog/Search/DoSearch');
                    }
                }
    
        state: VisualiseItem
            script:
                log('WhataCurrentItem?: ' + $session.currentItem + toPrettyString($session.currentItem));
                visualiseGoodsDetails($session.currentItem, true);
                
            state: GoBackToSearch
                q: назад
                q: * (не (то/это*/эту/эти*)/~другой/не нравится) * 
                script:
                    paginator().currentPage();  

        state: DoSearch
            q: ($hurryUpCommands/$agree) || fromState = ., onlyThisState = true
            script:
                var searchOpts = $temp.goodsCategory ? {GoodsCategoryId: $temp.goodsCategory} : {};
                paginator().display({
                    exitState: '/Catalog/Search',
                    iterator: new SearchPageIterator($session.textToSearch, {}),
                    processor: new GoodsPageProcessor()
                    });

            state: NoResults
                script:
                    $reactions.answer('К сожалению, по этому запросу нет результатов. Давайте поищем что-нибудь ещё.');     
                go!: /Catalog/Main

        state: Back
            q: назад || fromState = ../, onlyThisState = true
            script:
                if (paginator().getIterator().hasPrevious()) {
                    paginator().previousPage();
                } else if (paginator().containsState('categories')) {
                    paginator().restore('categories');
                    $reactions.transition({value: '/Catalog/Main/Category', deferred: true});
                } else {
                    $reactions.transition('/Catalog/Main');
                }
                    

        state: BackToCategories
            q: * (назад к категориям/вернись) * || fromState = ../, onlyThisState = true
            script:
                if (paginator().containsState('categories')) {
                    paginator().restore('categories');
                    $reactions.transition({value: '/Catalog/Main/Category', deferred: true});
                } else {
                    $reactions.transition('/Catalog/Main');
                }

    state: AddToCart
        q: * $add *  || fromState = /Catalog/Main/SearchInCategory/CheckButton/VisualiseItem, onlyThisState = true
        q: * $add *  || fromState = /Catalog/Search/VisualiseItem, onlyThisState = true
        q: $quantityModification || fromState = /Catalog/Main/SearchInCategory/CheckButton/VisualiseItem, onlyThisState = true
        q: $quantityModification || fromState = /Catalog/Search/VisualiseItem, onlyThisState = true
        script:
            if ($session.currentItem) {
                var itemId = $session.currentItem.ID;
                var numberExist = ($parseTree.quantityModification && $parseTree._Number);
                var itemQuantity = (numberExist) ? $parseTree._Number : 1; 

                if (typeof itemQuantity === 'string') {
                    itemQuantity = parseInt(itemQuantity.replace('+', '').replace('-', ''));
                }

                cart().add(itemId, itemQuantity); 
                $reactions.answer({value: "Товар добавлен в корзину.", tts: "Товар добавлен в корзину." });
                $temp.nextItem = true;
                $session.currentItem = cart().getItemById($session.currentItem.ID); //обновить на "корзинный формат"
                visualiseCurrentItem($session.currentItem);
                $reactions.buttons([{text: 'Оформить заказ', hide: true, transition: '/Cart/Show'}, {text: 'Заказать ещё', hide: true, transition: '/Catalog/Main'}]);            
            }


