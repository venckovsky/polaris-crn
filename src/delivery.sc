require: patterns.sc
  module = common

patterns:
    $extraHouseInfo = (корпус/к/кор/строение/стр/литера/лит/литер/л/с/дробь/черта/слеш/"/")
    $houseNumber = ($Number|$Number $AnyLetter|$Number $extraHouseInfo ($Number|$AnyLetter)) 
                    || converter = $converters.houseNumberConverter

    $change = (~изменить|~поменять|~сменить|~менять|~меняться|~поменяться|~измениться|~смена|~заменить|~замена)
    $now = (*быстр*|сейчас|немедленно|пулей|ближайш*)

theme: /Delivery
    state: Start
        q!: start delivery
        q: * тип достав* * 
        q: * $disagree * || fromState = /Delivery/Start/Foodbox, onlyThisState=true
        q: * $disagree * || fromState = /Delivery/Start/Address, onlyThisState=true
        script:
            $session.deliveryOptions = {};
            $reactions.answer('Выберите, пожалуйста, тип доставки:');
            $reactions.buttons([{text: 'Доставка по Москве', transition: '/Delivery/Start/deliveryMoscow'}, 
                                {text: 'Доставка по России', transition: '/Delivery/Start/ThroughoutRussia'},
                                {text: 'Самовывоз', transition: '/Delivery/Start/Pickup'}]);

        state: deliveryMoscow
            q: * (Доставка по Москве/в москв*/по москве/москва) * || fromState = /Delivery/Start, onlyThisState = true
            q: ($agree/подожду/жду/долго) || fromState = ../Address, onlyThisState = true
            script:
                $session.deliveryTypeId = 2;
                order().chooseDeliveryType($session.deliveryTypeId);
            go!: /Delivery/Start/Address


        state: ThroughoutRussia
            q: * (Доставка по России/не в москв*/не по москве/за пределами москвы/по рос*/$City) * || fromState = /Delivery/Start, onlyThisState = true
            q: ($agree/подожду/жду/долго) || fromState = ../ThroughoutRussia, onlyThisState = true
            script:
                $session.deliveryTypeId = 5;
                order().chooseDeliveryType($session.deliveryTypeId);
            go!: /Delivery/Start/Address

        state: Pickup
            q: * (сам*/самовывоз) * || fromState = /Delivery/Start, onlyThisState = true
            q: ($agree/подожду/жду/долго) || fromState = ../Pickup, onlyThisState = true
            script:
                $session.deliveryTypeId = 3;
                order().chooseDeliveryType($session.deliveryTypeId);
                $session.fullAddress = {};
                $session.fullAddress.street = ''; 
                $session.fullAddress.house = ''; 
                $session.fullAddress.flat = '';
            go!: /Delivery/Start/Address/Name

        state: Address
            a: Пожалуйста, назовите ваш адрес (улицу и дом).

            state: AddressInput
                q: $Text [$houseNumber [$Text]]  
                script:
                    $session.fullAddress = {};
                    if (!$parseTree.houseNumber) {
                        $session.rawAddressDct = {};
                        $reactions.transition('/Delivery/Start/Address/AddressByPiece/Town/Street');
                        } else if ($parseTree.Text && $parseTree.Text[1]) {
                            $session.rawAddress = $parseTree.Text[0].text + " " + $parseTree._houseNumber + " " + $parseTree.Text[1].text;
                        } else {
                            $session.rawAddress = $parseTree._Text + " " + $parseTree._houseNumber;
                        }
                go!: /Delivery/Start/Address/CheckAddress


            state: AddressByPiece
                # спрашиваем по кусочкам, чтобы наверняка
                q: * (ошиб*/(неправильн*/измени*/помен*/друг*) (адрес/[номер] дом*/улиц*)) * 
                script:
                    $session.inAddressByPiece = true;
                    $session.rawAddressDct = {};
                    $reactions.answer('Давайте попробуем ещё раз по частям.\nУточните, пожалуйста, ваш город или населённый пункт.');
                    $reactions.buttons([{text: 'Выбрать самовывоз', hide: true, transition: '/Delivery/Start/Pickup'}]);

                state: Town 
                    q: *
                    script:
                        $session.rawAddressDct.town = $parseTree.text;
                        $reactions.answer('Уточните, пожалуйста, улицу.');
                        $reactions.buttons({text: 'Назад', hide: true, transition: '../'});

                    state: Street
                        q: *
                        script:
                            $session.rawAddressDct.street = $parseTree.text;
                            $reactions.answer('Уточните, пожалуйста, номер дома.');
                            $reactions.buttons({text: 'Назад', hide: true, transition: '../'});
                          
                        state: NoHouse
                            q: *
                            a: Введите, пожалуйста, число.
                            go!: ../../Street

                        state: House
                            q: * $houseNumber *
                            script:
                                $session.rawAddressDct.house = $parseTree._houseNumber;
                                if ($session.rawAddressDct.town) {
                                    $session.rawAddress = $session.rawAddressDct.town + " " + $session.rawAddressDct.street + " " + $session.rawAddressDct.house;
                                } else {
                                    $session.rawAddress = $session.rawAddressDct.street + " " + $session.rawAddressDct.house;
                                }
                            go!: /Delivery/Start/Address/CheckAddress  


            state: CheckAddress
                script:
                    log('rawAddress: ' + $session.rawAddress);
                    YandexGeocoderUtils.getAddressObject($session.rawAddress);
                if: $session.addressesDct && Object.keys($session.addressesDct).length > 0
                    go!: ./AddressList
                elseif: $session.fullAddress && $session.fullAddress.lat && $session.fullAddress.lon && $session.fullAddress.house && $session.fullAddress.street
                    go!: /Delivery/Start/Address/GetFlatNumber
                else: 
                    go!: ./BadAddress            

                state: AddressList
                    script:
                        visualiseAddressList($session.addressesDct); 

                    state: ChosenAddress
                        script:
                            var geoObject = $session.addressesDct[$request.query];
                            $.session.fullAddress = YandexGeocoderUtils.parseGeoObject(geoObject);
                            $session.addressesDct = {};
                        go!: /Delivery/Start/Address/GetFlatNumber                      

                state: BadAddress
                    if: $session.inAddressByPiece === true
                        script:
                            $session.inAddressByPiece === true
                            $reactions.answer("К сожалению, не удалось найти данный адрес. Пожалуйста, выберите другой или самовывоз.");
                            $reactions.buttons([{text: 'Изменить адрес', hide: true, transition: '/Delivery/Start/AddressInput'}, {text: 'Выбрать самовывоз', hide: true, transition: '/Delivery/Start/Pickup'}]);    
                                
                    else:
                        a: Хм, не могу найти этот адрес.
                        go!: /Delivery/Start/Address/AddressByPiece   


            state: GetFlatNumber
                q: * (измен*/поменя*/друг*) ([номер] квартир*) * 
                script:
                    $reactions.answer('Укажите, пожалуйста, номер квартиры.');
                    $reactions.buttons([{text: 'Не до квартиры', hide: true, transition: './NoFlat'}, {text: 'Неправильный адрес', transition: '/Delivery/Start/Address/AddressByPiece'}]);

                state: Flat
                    q: * $Number *
                    script:
                        $session.fullAddress.flat = $parseTree.Number[0].value;
                    go!: /Delivery/Start/Address/GetFullAddress

                state: NoFlat
                    q: * (нет/не до квартир*/до подъезд*) *
                    script:
                        $session.fullAddress.flat = '-';
                    go!: /Delivery/Start/Address/GetFullAddress

                state: NotANumberOfFlat
                    q: * 
                    a: Что-то не похоже на число. Введите, пожалуйста, правильный номер квартиры!
                    go: ../


            state: GetFullAddress
                script:
                    var answer = 'Итак, ваш адрес: ' + $session.fullAddress.street + ', дом ' + $session.fullAddress.house;
                    if ($session.fullAddress.flat != '-') {
                        answer += ', кв. ' + $session.fullAddress.flat;
                    } 
                    answer += '. Верно?';
                    $reactions.answer(answer);
                    $reactions.buttons([{text: 'Подтвердить адрес', hide: true, transition: '/Delivery/Start/Address/Name'}, {text: 'Изменить адрес', hide: true, transition: '/Delivery/Start/Address'}]);

            state: Name
                a: Введите, пожалуйста, ваше имя.

                state: Name
                    q: *
                    script:
                        $session.userName = $parseTree.text;
                        $reactions.answer('Вы можете воспользоваться промокодом, если он у вас есть.');
                        $reactions.buttons([{text: 'Назад', hide: true, transition: '../'}, 
                                            {text: 'Пропустить', hide: true, transition: '/Order/OrderAdd'}, 
                                            {text: 'Управление промокодами', hide: true, transition: '/Order/Promocode'}]);
                state: setPromocode
                    q: $Number
                    script:
                        var promocode = $parseTree.Number[0].value;
                        var promocodeUse = order().usePromocode(promocode);
                        if (promocodeUse.isSuccessfulApiCall()) {
                            $session.promocode = promocode;
                            $reactions.transition('/Order/OrderAdd');
                        } else {
                            $reactions.answer('К сожалению, добавить промокод не удалось.');
                            $reactions.buttons([{text: 'Назад', hide: true, transition: '/Delivery/Start/Address/Name'}, 
                                                {text: 'Пропустить', hide: true, transition: '/Order/OrderAdd'}, 
                                                {text: 'Управление промокодами', hide: true, transition: '/Order/Promocode'}]);
                        }