patterns: 
    $PlusNumber = $regexp<\+\d+>
    $MinusNumber = $regexp<\-\d+>

    $delete = (~удалить|~убрать|*очист*)
    $add = (добав*/закажи/заказать/заказывай/добавить/полож*/в корзину/давай [[вот] это/её/его]/клади/беру/купи*/покупаю/накин*/плюс/+/прибав*/увелич*)
    $this = (это/ту/то/этот)
    $quantityModification = ((+1/(плюс/+/ещё) (1/один)):add |(-1/(минус/-) (один/1)):subtract |
                            (* (добав*/закажи/заказать/заказывай/добавить/полож*/давай/хочу/прибав*) ещё *):add | 
                            ($PlusNumber::Number:add | $MinusNumber::Number:subtract) |  
                            (* [$add] {ещ* * $Number} *):add |
                            {$add [ещ*] * $Number [штук]} :add | 
                            ($add [$this]):add | 
                            (* (убавь*/убавить/отними/отнять/уменьш*) * [$Number] *):subtract |
                            (* (убери*/убрать/удали/удалить) * $Number *):subtract |
                            (* $Number лишн* *):subtract |
                            (* (помен*/измени*/измени*/замени*) * $Number *):newQuantity)


theme: /Cart

    state: Show
        q!: * ((покажи|показать|перейти|перейди|открыть|открой) * ~корзина|что в ~корзина) *
        q!: ~корзина
        q!: * (сдела*/отправ*/зарегистрир*/оформ*) * [заказ/покупк*] * 
        q: * ничего * || fromState = /Catalog/Main
        q: закончить редактирование || fromState = /Cart
        q: * ((закончить/законч*/хватит/прекратит*/внест*/завершить) [редактиров*/изменен*]) * || fromState = /Cart/Modify, onlyThisState = true
        script:
            $temp.cartList = cart().lookup();
            if ($session.currentOrder && $session.currentOrder.EditableUntil && !dateIsPast($session.currentOrder.EditableUntil)) {
                visualisePreOrderCart($temp.cartList);
            } else {
                visualiseCart($temp.cartList);
            }
            

    state: Modify
        q: * (измени*/поменяй*) * (заказ/корзин*) * || fromState = /Catalog/AddToCart, onlyThisState=true 
        q!: * ($delete * из ~корзина|редактировать ~корзина) *
        q: * ($delete|~редактировать) * || fromState = /Cart/Show
        q: * {$delete $Text} * || fromState = ., onlyThisState=true
        script:
            $session.stateMode = 'inCart';
            var goodsList = cart().lookup();
            if (goodsList.length < 1) {
                $reactions.answer("Ваша корзина пуста.");
                $reactions.transition("/Catalog/Main");
            } else {
                visualiseCartModification(goodsList, getItemTransition);
                var getItemTransition = '/Cart/Modify/GetItem';
            } 
                        
        state: GetItem
            script:
                $session.currentItem = nameToItem[$request.query];
            go!: ../VisualiseItemModifyInfo

        state: VisualiseItemModifyInfo 
            script:
                visualiseCurrentItem($session.currentItem, '/Cart/Modify'); 
            
        state: VisualiseItemNewModifyInfo 
            script:
                visualiseModifiedItem($session.currentItem, '/Cart/Modify');

        state: QuantityModification
            script:
                visualiseItemForModification($session.currentItem, '/Cart/Modify/ModifyQuantity'); 

        state: ModifyQuantity
            q: $quantityModification || fromState = /Cart/Modify
            q: $quantityModification || fromState = /Cart/Modify/VisualiseItemModifyInfo
            q: $quantityModification || fromState = /Catalog/AddToCart, onlyThisState = true
            script:
                if ($session.currentItem) {
                    $temp.nextItem = true;
                    var numberExist = $parseTree.quantityModification;
                    var newQuantity = numberExist; 

                    if (typeof newQuantity === 'string') {
                        newQuantity = parseInt(newQuantity.replace('+', '').replace('-', ''));
                    }

                    if (newQuantity === 0) {
                        $reactions.answer('Вы не изменили количество товара.');
                    } else {
                        var mode = $parseTree._Root;
                        modifyQuantity($session.currentItem, mode, newQuantity);
                    }
                         
                }
            if: $session.stateMode === 'inCart'
                go!: /Cart/Modify/VisualiseItemNewModifyInfo
            else:        
                go!: /Catalog/Main


            state: IfDelete
                q:  * (удали*/убери*/не нужно/не нужен/не нужна) * || fromState = /Cart/Modify/VisualiseItemModifyInfo, onlyThisState = true
                q:  * (удали*/убери*/не нужно/не нужен/не нужна) * || fromState = /Cart/Modify/ModifyQuantity, onlyThisState = true
                q:  * (удали*/убери*/не нужно/не нужен/не нужна) * || fromState = /Catalog/Main, onlyThisState = true
                script:
                    var currentItem = $session.currentItem;
                    $reactions.answer("Вы хотите удалить товар " + currentItem.NAME + "?");
                    if ($session.stateMode === "inCart") {
                        $reactions.buttons([{text: 'Да', hide: true, transition: '/Cart/Modify/Delete'}, {text: 'Нет', hide: true, transition: '/Cart/Modify/ModifyQuantity'}]);
                    } else {
                        $reactions.buttons([{text: 'Да', hide: true, transition: '/Cart/Modify/Delete'}, {text: 'Нет', hide: true, transition: '/Catalog/Main'}])
                    }
                    
                state: Yes
                    q: $agree
                    go!: /Cart/Modify/Delete
                
                state: No
                    q: $disagree    
                    go!: /Cart/Modify/ModifyQuantity

        state: Delete
            script:
                $reactions.answer("Товар удалён.");
                cart().delete($session.currentItem.ID);
                if ($session.stateMode === "inCart") { 
                    $reactions.transition('/Cart/Show');
                } else {
                    $reactions.transition('/Catalog/Main');
                }


    state: ConfirmDeletion
        q!: * ($delete корзину| $delete (все из|всю) ~корзина) *
        a: Вы действительно хотите очистить корзину?
        buttons: 
            {text: "Да", transition: "./Yes", hide: true}              
            {text: "Нет", transition: "./No", hide: true}

        state: Yes
            q: $agree
            script:
                cart().clear();
                $reactions.answer("Ваша корзина пуста.");
                
            go!: /Catalog/Main
                
        state: No
            q: $disagree    
            go!: /Cart/Show
        
    state: ModifyInCart
        script:
            $reactions.answer("Вы хотите изменить количество товара или удалить из корзины?");
            $reactions.buttons([{text: 'Изменить', hide: true, transition: '/Cart/Modify/QuantityModification'}, {text: 'Удалить', hide: true, transition: '/Cart/Modify/ModifyQuantity/IfDelete'}]);

    state: ConfirmCart 
        q: * (подтвердить/подтверждаю/все (верно/правильно)) * || fromState = /Cart/Show, onlyThisState=true
        q: * (доставк*/~доставить)*
        script:
            var checkMinCost = checkOrder();
            if (checkMinCost && checkMinCost.type === 'lessMinCostFoodbox') {
                $reactions.answer(checkMinCost.answer);
                $reactions.buttons([{text: 'Заказать ещё', hide: true, transition: '/Catalog/Main'},
                                    {text: 'Изменить', hide: true, transition: '/Cart/Modify'}, 
                                    {text: 'Отменить заказ', hide: true, transition: '/Order/Break'}]);
                } else {
                $reactions.transition('/Authorisation/PhoneNumber');
                }


    state: LookupTimeout
        q: * ($hurryUpCommands/$agree) * || fromState = ., onlyThisState=true
        script:
            $reactions.answer("Открываю корзину. Подождёте ещё чуть-чуть?");
            $reactions.buttons([{text: 'Да', hide: true, transition: '/Cart/Show'}, {text: 'Посмотреть ещё товары', hide: true, transition: '/Catalog/Main'}]);
