patterns:
    $nextPage = (еще|дальше|вперед)

theme: /

    state: Paginator || modal = true

        state: More 
            q: [~показать/давай] $nextPage
            script:
                paginator().nextPage();

        state: NoMore
            a: Нет результатов.        

        state: CatchAll
            q: *
            script:
                paginator().exit($parseTree.text)
