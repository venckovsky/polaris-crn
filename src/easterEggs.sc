theme: /EasterEggs

    state: Kill time || noContext = true
        q!: * я хочу убить время *
        q: * я хочу убить время * || fromState = /Paginator
        a: Время очень не любит, когда его убивают. Давай лучше потратим его с пользой. Как насчет скляночки "Выпей меня"?
        script:
            $reactions.buttons([{text: 'Звучит заманчиво', url: 'https://www.utkonos.ru/action/detail/24599'}]);

    state: Eat me || noContext = true
        q!: * съешь меня съешь меня съешь меня *
        q: * съешь меня съешь меня съешь меня * || fromState = /Paginator
        a: Всё, что сказано три раза, становится истиной. Я слышала, что Белый Кролик знает, где найти то, что ты ищешь.
        script:
            $reactions.buttons([{text: 'Следовать за Белым Кроликом', url: 'https://www.utkonos.ru/action/detail/24599'}]);